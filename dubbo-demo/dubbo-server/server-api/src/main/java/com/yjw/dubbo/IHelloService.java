package com.yjw.dubbo;

public interface IHelloService {

    String sayHello(String name);
}
