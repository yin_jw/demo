package com.yjw.dubbo;

public class HelloServiceImpl implements IHelloService {

    @Override
    public String sayHello(String name) {
        return "hello " + name;
    }
}
