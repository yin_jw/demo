package com.yjw.dubbo;

import com.alibaba.dubbo.container.Main;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Hello world!
 */
public class Bootstrap {

    public static void main(String[] args) {
        containerStart(new String[]{"spring", "log4j"});
    }

    /**
     * 使用Spring上下文加载Dubbo配置
     *
     * @throws IOException
     */
    public static void nativeStart() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("META-INF/spring/dubbo-server.xml");
        context.start();
        // 阻塞当前进程
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 容器启动
     *
     * @param args
     */
    public static void containerStart(String[] args) {
        Main.main(args);
    }

}
