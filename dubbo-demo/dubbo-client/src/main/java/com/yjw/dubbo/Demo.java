package com.yjw.dubbo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 */
public class Demo {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("META-INF/spring/dubbo-client.xml");
        IHelloService helloService = (IHelloService) context.getBean("helloService");
        System.out.println(helloService.sayHello("张三"));
    }
}
