package com.yjw.zookeeper.native_api;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class ConnectionDemo {

    public static void main(String[] args) {
        ZooKeeper zooKeeper = null;
        try {
            CountDownLatch countDownLatch = new CountDownLatch(1);
            zooKeeper = new ZooKeeper("192.168.202.18:2181,192.168.202.49:2181,192.168.202.50:2181",
                    4000, new Watcher() {
                @Override
                public void process(WatchedEvent event) {
                    // 收到服务端响应时间，连接成功
                    if (Event.KeeperState.SyncConnected == event.getState()) {
                        countDownLatch.countDown();
                    }
                }
            });
            countDownLatch.await();
            System.out.println(zooKeeper.getState());

            // 创建节点
            zooKeeper.create("/zk-persis", "0".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            Thread.sleep(1000);

            // 获取节点
            Stat stat = new Stat();
            byte[] bytes = zooKeeper.getData("/zk-persis", false, stat);
            System.out.println(new String(bytes));

            // 更新节点
            zooKeeper.setData("/zk-persis", "1".getBytes(), stat.getVersion());

            // 获取节点
            byte[] bytes1 = zooKeeper.getData("/zk-persis", false, stat);
            System.out.println(new String(bytes1));

            // 删除节点
            zooKeeper.delete("/zk-persis", stat.getVersion());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        } finally {
            if (zooKeeper != null) {
                try {
                    zooKeeper.close();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
