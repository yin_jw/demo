package com.yjw.zookeeper.distributed_lock;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class CuratorDLock {

    public static void main(String[] args) {
        // 建立连接
        CuratorFramework curatorFramework = CuratorFrameworkFactory.builder()
                .connectString("192.168.202.18:2181,192.168.202.49:2181,192.168.202.50:2181")
                .sessionTimeoutMs(4000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .namespace("curator")
                .build();

        curatorFramework.start();

        InterProcessMutex interProcessMutex = new InterProcessMutex(curatorFramework, "/locks");
        try {
            interProcessMutex.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
