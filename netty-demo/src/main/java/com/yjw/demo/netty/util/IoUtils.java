package com.yjw.demo.netty.util;

import java.io.Closeable;
import java.io.IOException;

/**
 * @author: James Yin
 * @description:
 * @date: 2/10/2020 10:11 PM
 */
public class IoUtils {

    private IoUtils() {
    }

    /**
     * 关闭 IO 资源
     *
     * @param closeables IO 资源
     */
    public static void close(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
