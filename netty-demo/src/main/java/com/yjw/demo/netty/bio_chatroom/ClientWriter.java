package com.yjw.demo.netty.bio_chatroom;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * 客户端监听控制台输入
 * 
 * @author yinjianwei
 * @date 2018/11/15
 */
public class ClientWriter implements Runnable {

    private Socket socket;

    public ClientWriter(Socket socket) {
        super();
        this.socket = socket;
    }

    @Override
    public void run() {
        Scanner sc = null;
        try {
            sc = new Scanner(System.in);
            while (true) {
                String message = sc.nextLine();
                PrintWriter pw = null;
                pw = new PrintWriter(socket.getOutputStream(), true);
                pw.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (sc != null) {
                sc.close();
                sc = null;
            }
        }
    }

}
