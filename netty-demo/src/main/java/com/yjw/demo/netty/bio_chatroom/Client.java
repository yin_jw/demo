package com.yjw.demo.netty.bio_chatroom;

import java.io.IOException;
import java.net.Socket;

/**
 * 客户端
 * 
 * @author yinjianwei
 * @date 2018/11/15
 */
public class Client {

    private static final String DEFAULT_SERVER_IP = "127.0.0.1";
    private static final int DEFAULT_SERVER_PORT = 8888;

    public static void send() {
        Socket socket = null;
        try {
            socket = new Socket(DEFAULT_SERVER_IP, DEFAULT_SERVER_PORT);
            // 接收控制台输入，并发送服务端
            new Thread(new ClientWriter(socket)).start();
            // 读取服务端数据
            new Thread(new ClientReader(socket)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        send();
    }
}
