<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!-- 
    SQL映射文件有很少的几个顶级元素（按照它们应该被定义的顺序）：
    cache：给定命名空间的缓存配置。
    cache-ref：其他命名空间缓存配置的引用。
    resultMap：是最复杂也是最强大的元素，用来描述如何从数据库结果集中来加载对象。
    parameterMap：已废弃！老式风格的参数映射。内联参数是首选,这个元素可能在将来被移除，这里不会记录。
    sql：可被其他语句引用的可重用语句块。
    insert：映射插入语句
    update：映射更新语句
    delete：映射删除语句
    select：映射查询语句
 -->
<mapper namespace="com.yjw.demo.mybatis.biz.dao.StudentDao">

    <!-- 二级缓存 -->
    <cache eviction="LRU" flushInterval="100000" size="1024" readOnly="true" />

    <!--
	   <resultMap>
	       <constructor> —— 用于在实例化类时，注入结果到构造方法中
	           <idArg/> —— ID 参数，标记出作为ID的结果可以帮助提高整体性能
	           <arg/> —— 将被注入到构造方法的一个普通结果
            </constructor>
            <id/> —— 一个ID结果，标记出作为ID的结果可以帮助提高整体性能
            <result/> —— 注入到字段或JavaBean属性的普通结果
            <association/> —— 代表一对一关系，嵌套结果映射，关联可以指定为一个resultMap元素，或者引用一个
            <collection/> —— 代表一对多关系，嵌套结果映射，集合可以指定为一个resultMap元素，或者引用一个
            <discriminator> —— 是鉴别器，它可以根据实际选择采用哪个类作为实例，允许你根据特定的条件去关联不同的结果集
	           <case/>
            </discriminator>
	   </resultMap>
	-->

    <resultMap id="BaseResultMap" type="studentDO">
        <id column="id" jdbcType="BIGINT" property="id" />
        <result column="name" jdbcType="VARCHAR" property="name" />
        <result column="sex" jdbcType="TINYINT" property="sex" />
        <!--<result column="sex" jdbcType="TINYINT" property="sex"
                typeHandler="com.yjw.demo.mybatis.common.type.SexEnumTypeHandler"/>-->
        <result column="selfcard_no" jdbcType="BIGINT" property="selfcardNo" />
        <result column="note" jdbcType="VARCHAR" property="note" />
    </resultMap>
    
    <sql id="Base_Column_List">
        id, name, sex, selfcard_no, note
    </sql>

    <!-- 定制参数 -->
    <!--
        <sql id="Student_Column_List">
            ${prefix}.id, ${prefix}.name, ${prefix}.sex, ${prefix}.selfcard_no, ${prefix}.note
        </sql>

        <select id="listByConditions" parameterType="studentQuery" resultMap="BaseResultMap">
            select
            <include refid="Lecture_Column_List">
                <property name="prefix" value="t" />
            </include>
            from t_student t
            <where>
                <if test="name != null and name != ''">
                    AND name LIKE CONCAT('%', #{name}, '%')
                </if>
            </where>
        </select>
     -->

    <!-- 根据主键查询学生 -->
    <select id="getByPrimaryKey" parameterType="java.lang.Long" resultMap="BaseResultMap">
        select
        <include refid="Base_Column_List" />
        from t_student
        where id = #{id,jdbcType=BIGINT}
    </select>

    <!-- 根据条件获取学生信息-->
    <select id="listByConditions" parameterType="studentQuery" resultMap="BaseResultMap">
        select
        <include refid="Base_Column_List" />
        from t_student
        <where>
            <if test="ids != null and ids.size() > 0">
                AND id IN
                <foreach collection="ids" item="item" open="(" close=")" separator=",">
                    #{item}
                </foreach>
            </if>
            <if test="name != null and name != ''">
                AND name LIKE CONCAT('%', #{name}, '%')
            </if>
            <if test="sex != null">
                AND sex = #{sex}
            </if>
            <if test="selfcardNo != null">
                AND selfcard_no = #{selfcardNo}
            </if>
        </where>
    </select>

    <!-- 使用 Map 传递参数 -->
    <select id="listByMap" parameterType="map" resultMap="BaseResultMap">
        select
        <include refid="Base_Column_List" />
        from t_student
        <where>
            <if test="ids != null and ids.size() > 0">
                AND id IN
                <foreach collection="ids" item="item" open="(" close=")" separator=",">
                    #{item}
                </foreach>
            </if>
            <if test="name != null and name != ''">
                AND name LIKE CONCAT('%', #{name}, '%')
            </if>
            <if test="sex != null">
                AND sex = #{sex}
            </if>
            <if test="selfcardNo != null">
                AND selfcard_no = #{selfcardNo}
            </if>
        </where>
    </select>

    <!-- 使用注解方式传递参数 -->
    <select id="listByParamAnnotation" resultMap="BaseResultMap">
        select
        <include refid="Base_Column_List" />
        from t_student
        <where>
            <if test="name != null and name != ''">
                AND name LIKE CONCAT('%', #{name}, '%')
            </if>
            <if test="sex != null">
                AND sex = #{sex}
            </if>
        </where>
    </select>
    
    <!-- 
        bind元素
        <select id="listByConditions" parameterType="studentQuery" resultMap="BaseResultMap">
            <bind name="_name" value="'%' + name + '%'"/>
            select
            <include refid="Base_Column_List" />
            from t_student
            <where>
                <if test="ids != null and ids.size() > 0">
                    AND id IN
                    <foreach collection="ids" item="item" open="(" close=")" separator=",">
                        #{item}
                    </foreach>
                </if>
                <if test="name != null and name != ''">
                    AND name LIKE #{_name}
                </if>
                <if test="sex != null">
                    AND sex = #{sex}
                </if>
                <if test="selfcardNo != null">
                    AND selfcard_no = #{selfcardNo}
                </if>
            </where>
        </select>
     -->
    
    <!-- 
        choose、when、otherwise元素
        1.当学生姓名不为空，则只用学生姓名作为条件查询
        2.当学生性别不为空，则只用学生性别作为条件查询
        3.当学生姓名和学生性别都为空，则要求学生学生证件号不为空
    -->
    <!-- 
        <select id="listByConditions" parameterType="studentQuery" resultMap="BaseResultMap">
            select
            <include refid="Base_Column_List" />
            from t_student
            <where>
                <choose>
                    <when test="name != null and name != ''">
                        AND name LIKE CONCAT('%', #{name}, '%')
                    </when>
                    <when test="sex != null">
                        AND sex = #{sex}
                    </when>
                    <otherwise>
                        AND selfcard_no is not null
                    </otherwise>
                </choose>
            </where>
        </select>
    -->

    <!-- 配置两个属性（keyProperty、useGeneratedKeys）获取表中生成的自增长主键。 -->
    <!-- keyProperty：表示以哪个列作为属性的主键，不能和keyColumn同时使用，如果你是联合主键可以用逗号将其隔开。 -->
    <!-- useGeneratedKeys：这会令MyBatis使用JDBC的getGeneratedKeys方法来取出由数据库内部生成的主键，例如，MySQL和SQL Server自动递增字段，
        Oracle的序列等，但是使用它就必须要给keyProperty或者keyColumn赋值，取值为布尔值，true/false，默认值为false。 -->
    <!-- 新增：存在自增主键 -->
    <insert id="insertByAutoInc" parameterType="studentDO" keyProperty="id" useGeneratedKeys="true">
        insert into t_student (name, sex, selfcard_no, note)
        values (
            #{name,jdbcType=VARCHAR},
            #{sex,jdbcType=TINYINT},
            <!-- #{sex,jdbcType=TINYINT,typeHandler=com.yjw.demo.mybatis.common.type.SexEnumTypeHandler}, -->
            #{selfcardNo,jdbcType=BIGINT},
            #{note,jdbcType=VARCHAR}
        )
    </insert>

    <!-- 假设我们取消表t_student的id自增的规则，我们的要求是：如果表t_student没有记录，则我们需要设置id=1，否则我们就取最大id加2，来设置新的主键。 -->
    <!-- 新增：不存在自增主键 -->
    <insert id="insertByNoAutoInc" parameterType="studentDO">
        <selectKey keyProperty="id" resultType="long" order="BEFORE">
            select if(max(id) is null, 1, max(id) + 2) as newId from t_student
        </selectKey>
        insert into t_student (id, name, sex, selfcard_no, note)
        values (
            #{id,jdbcType=BIGINT},
            #{name,jdbcType=VARCHAR},
            #{sex,jdbcType=TINYINT},
            #{selfcardNo,jdbcType=BIGINT},
            #{note,jdbcType=VARCHAR}
        )
    </insert>

    <insert id="insertSelective" parameterType="studentDO" keyProperty="id" useGeneratedKeys="true">
        insert into t_student
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="name != null and name != ''">
                name,
            </if>
            <if test="sex != null">
                sex,
            </if>
            <if test="selfcardNo != null">
                selfcard_no,
            </if>
            <if test="note != null and note != ''">
                note,
            </if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <if test="name != null and name != ''">
                #{name,jdbcType=VARCHAR},
            </if>
            <if test="sex != null">
                #{sex,jdbcType=TINYINT},
            </if>
            <if test="selfcardNo != null">
                #{selfcardNo,jdbcType=BIGINT},
            </if>
            <if test="note != null and note != ''">
                #{note,jdbcType=VARCHAR},
            </if>
        </trim>
    </insert>
    
    <!-- 注意：批量新增的操作需要确保list中必须有值 -->
    <!-- 批量新增：存在自增主键 -->
    <insert id="batchInsertByAutoInc" parameterType="list" keyProperty="id" useGeneratedKeys="true">
    	insert into t_student (name, sex, selfcard_no, note)
        values 
        <foreach collection="list" item="item" index="index" separator=",">
	        (
	            #{item.name,jdbcType=VARCHAR},
	            #{item.sex,jdbcType=TINYINT},
	            #{item.selfcardNo,jdbcType=BIGINT},
	            #{item.note,jdbcType=VARCHAR}
	        )
	   	</foreach>
    </insert>

    <!-- 注意：批量新增的操作需要确保list中必须有值 -->
    <!-- 批量新增：不存在自增主键 -->
    <insert id="batchInsertByNoAutoInc" parameterType="list">
        <selectKey keyProperty="id" resultType="long" order="BEFORE">
            select if(max(id) is null, 1, max(id) + 2) as newId from t_student
        </selectKey>
        insert into t_student (name, sex, selfcard_no, note)
        values
        <foreach collection="list" item="item" index="index" separator=",">
            (
            #{item.name,jdbcType=VARCHAR},
            #{item.sex,jdbcType=TINYINT},
            #{item.selfcardNo,jdbcType=BIGINT},
            #{item.note,jdbcType=VARCHAR}
            )
        </foreach>
    </insert>
    
    <update id="updateByPrimaryKey" parameterType="studentDO">
        update t_student 
        set 
            name = #{name,jdbcType=VARCHAR},
            sex = #{sex,jdbcType=TINYINT},
            selfcard_no = #{selfcardNo,jdbcType=BIGINT},
            note = #{note,jdbcType=VARCHAR}
        where id = #{id,jdbcType=BIGINT}
    </update>
    
    <update id="updateByPrimaryKeySelective" parameterType="studentDO">
        update t_student
        <set>
            <if test="name != null and name != ''">
                name = #{name,jdbcType=VARCHAR},
            </if>
            <if test="sex != null">
                sex = #{sex,jdbcType=TINYINT},
            </if>
            <if test="selfcardNo != null">
                selfcard_no = #{selfcardNo,jdbcType=BIGINT},
            </if>
            <if test="note != null and note != ''">
                note = #{note,jdbcType=VARCHAR},
            </if>
        </set>
        where id = #{id,jdbcType=BIGINT}
    </update>

    <!-- 批量更新 -->
    <!-- 注意：批量更新的操作需要确保list中必须有值 -->
    
    <delete id="deleteByPrimaryKey" parameterType="java.lang.Long">
        delete from t_student where id = #{id,jdbcType=BIGINT}
    </delete>

    <delete id="deleteByConditions" parameterType="studentQuery">
        delete from t_student
        <where>
            <if test="name != null and name != ''">
                AND name LIKE CONCAT('%', #{name}, '%')
            </if>
            <if test="sex != null">
                AND sex = #{sex}
            </if>
            <if test="selfcardNo != null">
                AND selfcard_no = #{selfcardNo}
            </if>
        </where>
    </delete>

    <!-- 联合查询：嵌套查询 -->
    <resultMap id="studentMap1" type="studentDO">
        <id column="id" jdbcType="BIGINT" property="id" />
        <result column="name" jdbcType="VARCHAR" property="name" />
        <result column="sex" jdbcType="TINYINT" property="sex"
                typeHandler="com.yjw.demo.mybatis.common.type.SexEnumTypeHandler"/>
        <result column="selfcard_no" jdbcType="BIGINT" property="selfcardNo" />
        <result column="note" jdbcType="VARCHAR" property="note" />
        <!--
        property：映射数据库列的字段或属性。如果JavaBean的属性与给定的名称匹配，就会使用匹配的名字。否则，MyBatis将搜索给定名称的字段。两种情况下您都可以使用逗点的属性形式。比如，您可以映射到”username”，也可以映射到更复杂点的”address.street.number”。
        column：数据库的列名或者列标签别名。与传递给resultSet.getString(columnName)的参数名称相同。注意： 在处理组合键时，您可以使用column= “{prop1=col1,prop2=col2}”这样的语法，设置多个列名传入到嵌套查询语句。这就会把prop1和prop2设置到目标嵌套选择语句的参数对象中。
        javaType：完整java类名或别名(参考上面的内置别名列表)。如果映射到一个JavaBean，那MyBatis通常会自行检测到。然而，如果映射到一个HashMap，那您应该明确指定javaType来确保所需行为。
        jdbcType：支持的JDBC类型列表中列出的JDBC类型。这个属性只在insert,update 或delete的时候针对允许空的列有用。JDBC 需要这项，但MyBatis不需要。如果您直接编写JDBC代码，在允许为空值的情况下需要指定这个类型。
        typeHandler：我们已经在文档中讨论过默认类型处理器。使用这个属性可以重写默认类型处理器。它的值可以是一个TypeHandler实现的完整类名，也可以是一个类型别名
        select：通过这个属性，通过ID引用另一个加载复杂类型的映射语句。从指定列属性中返回的值，将作为参数设置给目标select语句。注意：在处理组合键时，您可以使用column=”{prop1=col1,prop2=col2}”这样的语法，设置多个列名传入到嵌套语句。这就会把prop1和prop2设置到目标嵌套语句的参数对象中。
        resultMap：一个可以映射联合嵌套结果集到一个适合的对象视图上的ResultMap。这是一个替代的方式去调用另一个select语句。它允许您去联合多个表到一个结果集里。这样的结果集可能包括冗余的、重复的需要分解和正确映射到一个嵌套对象视图的数据组。简言之，MyBatis让您把结果映射‘链接’到一起，用来处理嵌套结果。
        fetchType： 设置局部延迟加载，它有两个取值范围，即eager和lazy。它的默认值取决于你在配置文件settings的配置，如果没有配置它，默认是eager，一旦配置了，全局配置就会被他们覆盖
        -->
        <!-- 嵌套查询：一对一级联 -->
        <association property="studentSelfcard" column="{studentId=id}"
                     select="com.yjw.demo.mybatis.biz.dao.StudentSelfcardDao.listByConditions" />

        <!-- 嵌套查询：一对多级联 -->
        <collection property="studentLectures" column="{studentId=id}"
                    select="com.yjw.demo.mybatis.biz.dao.StudentLectureDao.listByConditions" />

        <!-- 嵌套查询：鉴别器 -->
        <!-- discriminator：使用结果值来决定使用哪个 resultMap -->
        <!-- case：基于某些值的结果映射 -->
        <discriminator javaType="int" column="sex">
            <case value="1" resultMap="maleStudentMap1" />
            <case value="2" resultMap="femaleStudentMap1" />
        </discriminator>
    </resultMap>

    <!-- 男 -->
    <resultMap id="maleStudentMap1" type="maleStudentDO" extends="studentMap1">
        <collection property="studentHealthMales" column="{studentId=id}"
                    select="com.yjw.demo.mybatis.biz.dao.StudentHealthMaleDao.listByConditions" />
    </resultMap>

    <!-- 女 -->
    <resultMap id="femaleStudentMap1" type="femaleStudentDO" extends="studentMap1">
        <collection property="studentHealthFemales" column="{studentId=id}"
                    select="com.yjw.demo.mybatis.biz.dao.StudentHealthFemaleDao.listByConditions" />
    </resultMap>


    <select id="listStudentByNestingQuery" resultMap="studentMap1">
        select * from t_student
    </select>

    <!--
        ofType
    -->
    <!-- 联合查询：嵌套结果 -->
    <resultMap id="studentMap2" type="studentDO">
        <id property="id" column="id"/>
        <result property="name" column="name"/>
        <result property="selfcardNo" column="selfcard_no"/>
        <result property="note" column="note"/>
        <association property="studentSelfcard" javaType="studentSelfcardDO">
            <result property="id" column="ssid"/>
            <result property="nativePlace" column="native_place"/>
            <result property="issueDate" column="issue_date"/>
            <result property="endDate" column="end_date"/>
            <result property="note" column="ssnote"/>
        </association>
        <collection property="studentLectures" ofType="studentLectureDO">
            <result property="id" column="slid"/>
            <result property="grade" column="grade"/>
            <result property="note" column="slnote"/>
            <association property="lecture" javaType="lectureDO">
                <result property="id" column="lid"/>
                <result property="lectureName" column="lecture_name"/>
                <result property="note" column="lnote"/>
            </association>
        </collection>
        <discriminator javaType="int" column="sex">
            <case value="1" resultMap="maleStudentMap2"/>
            <case value="2" resultMap="femaleStudentMap2"/>
        </discriminator>
    </resultMap>

    <!-- 男 -->
    <resultMap id="maleStudentMap2" type="maleStudentDO" extends="studentMap2">
        <collection property="studentHealthMales" ofType="studentHealthMaleDO">
            <id property="id" column="hid"/>
            <result property="checkDate" column="check_date"/>
            <result property="heart" column="heart"/>
            <result property="liver" column="liver"/>
            <result property="spleen" column="spleen"/>
            <result property="lung" column="lung"/>
            <result property="kidney" column="kidney"/>
            <result property="prostate" column="prostate"/>
            <result property="note" column="shnote"/>
        </collection>
    </resultMap>

    <!-- 女 -->
    <resultMap id="femaleStudentMap2" type="femaleStudentDO" extends="studentMap2">
        <collection property="studentHealthFemales" ofType="studentHealthFemaleDO">
            <id property="id" column="hid"/>
            <result property="checkDate" column="check_date"/>
            <result property="heart" column="heart"/>
            <result property="liver" column="liver"/>
            <result property="spleen" column="spleen"/>
            <result property="lung" column="lung"/>
            <result property="kidney" column="kidney"/>
            <result property="uterus" column="uterus"/>
            <result property="note" column="shnote"/>
        </collection>
    </resultMap>

    <select id="listStudentByNestingResult" resultMap="studentMap2">
        SELECT s.id,s.name,s.sex,s.note,s.selfcard_no,
            if(sex=1,shm.id,shf.id) AS hid,
            if(sex=1,shm.check_date,shf.check_date) AS check_date,
            if(sex=1,shm.heart,shf.heart) AS heart,
            if(sex=1,shm.liver,shf.liver) AS liver,
            if(sex=1,shm.spleen,shf.spleen) AS spleen,
            if(sex=1,shm.lung,shf.lung) AS lung,
            if(sex=1,shm.kidney,shf.kidney) AS kidney,
            if(sex=1,shm.note,shf.note) AS shnote,
            shm.prostate,shf.uterus,
            ss.id AS ssid,ss.native_place,
            ss.issue_date,ss.end_date,ss.note AS ssnote,
            sl.id AS slid,sl.grade,sl.note AS slnote,
            l.lecture_name,l.note AS lnote
        FROM t_student s
        LEFT JOIN t_student_health_male shm ON s.id=shm.student_id
        LEFT JOIN t_student_health_female shf ON s.id = shf.student_id
        LEFT JOIN t_student_selfcard ss ON s.id = ss.student_id
        LEFT JOIN t_student_lecture sl ON s.id=sl.student_id
        LEFT JOIN t_lecture l ON sl.lecture_id = l.id
    </select>
</mapper>