package com.yjw.demo.mybatis.biz.dao;

import java.util.List;

import com.yjw.demo.mybatis.biz.pojo.entity.StudentHealthMaleDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentHealthMaleQuery;

/**
 * 男学生体检
 * 
 * @author yinjianwei
 * @date 2018/09/26
 */
public interface StudentHealthMaleDao {

    /**
     * 根据主键查询男学生体检
     * 
     * @param id
     * @return
     */
    StudentHealthMaleDO getByPrimaryKey(Long id);

    /**
     * 根据条件获取男学生体检信息
     * 
     * @param studentHealthMaleQuery
     * @return
     */
    List<StudentHealthMaleDO> listByConditions(StudentHealthMaleQuery studentHealthMaleQuery);

    /**
     * 根据条件计算男学生体检数量
     * 
     * @param studentHealthMaleQuery
     * @return
     */
    int countByConditions(StudentHealthMaleQuery studentHealthMaleQuery);

    /**
     * 新增男学生体检信息
     * 
     * @param studentHealthMaleDO
     * @return
     */
    int insert(StudentHealthMaleDO studentHealthMaleDO);

    /**
     * 选择性新增男学生体检信息
     * 
     * @param studentHealthMaleDO
     * @return
     */
    int insertSelective(StudentHealthMaleDO studentHealthMaleDO);

    /**
     * 根据主键更新男学生体检信息
     * 
     * @param studentHealthMaleDO
     * @return
     */
    int updateByPrimaryKey(StudentHealthMaleDO studentHealthMaleDO);

    /**
     * 根据主键选择性更新男学生体检信息
     * 
     * @param studentHealthMaleDO
     * @return
     */
    int updateByPrimaryKeySelective(StudentHealthMaleDO studentHealthMaleDO);

    /**
     * 根据主键删除男学生体检数据
     * 
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据条件删除男学生体检数据
     * 
     * @param studentHealthMaleQuery
     * @return
     */
    int deleteByConditions(StudentHealthMaleQuery studentHealthMaleQuery);
}