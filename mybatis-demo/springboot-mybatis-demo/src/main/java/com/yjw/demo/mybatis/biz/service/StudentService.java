package com.yjw.demo.mybatis.biz.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import com.yjw.demo.mybatis.biz.dao.StudentDao;
import com.yjw.demo.mybatis.biz.pojo.entity.StudentDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.biz.pojo.vo.StudentVO;
import com.yjw.demo.mybatis.common.constant.ErrorCodeEnum;
import com.yjw.demo.mybatis.common.exception.ServiceException;
import com.yjw.demo.mybatis.common.page.PageHelper;
import com.yjw.demo.mybatis.common.pojo.PageResult;
import com.yjw.demo.mybatis.common.util.BeanMapperUtils;
import com.yjw.demo.mybatis.common.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 学生 service
 *
 * @author yinjianwei
 * @date 2017/12/12
 */
@Service
public class StudentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentService.class);

    @Autowired
    private StudentDao studentDao;

    /**
     * 根据条件筛选学生数据
     *
     * @param studentQuery 查询条件
     * @return
     * @throws ServiceException
     */
    public List<StudentVO> listStudentByConditions(StudentQuery studentQuery) {
        List<StudentDO> studentDOs = studentDao.listByConditions(studentQuery);
        List<StudentVO> studentVOs = Lists.newArrayListWithCapacity(studentDOs.size());
        try {
            studentVOs = BeanMapperUtils.mapListByJson(studentDOs, StudentDO.class, null, null);
        } catch (IOException e) {
            LOGGER.error(ErrorCodeEnum.COPY_PROPERTIES_ERROR.getMessage(), e);
            throw new ServiceException(ErrorCodeEnum.COPY_PROPERTIES_ERROR.getCode(),
                ErrorCodeEnum.COPY_PROPERTIES_ERROR.getMessage(), e);
        }
        return studentVOs;
    }

    /**
     * 根据条件筛选学生分页数据
     *
     * @param studentQuery 查询条件
     * @return
     * @throws ServiceException
     */
    public PageResult<StudentVO> pageStudentByConditions(StudentQuery studentQuery) {
        PageHelper.startPage(studentQuery.getPageNum(), studentQuery.getPageSize());
        List<StudentDO> studentsPage = studentDao.listByConditions(studentQuery);
        PageResult<StudentVO> studentVOsPage;
        try {
            studentVOsPage = BeanMapperUtils.mapPageByJson(studentsPage);
        } catch (IOException e) {
            LOGGER.error(ErrorCodeEnum.COPY_PROPERTIES_ERROR.getMessage(), e);
            throw new ServiceException(ErrorCodeEnum.COPY_PROPERTIES_ERROR.getCode(),
                    ErrorCodeEnum.COPY_PROPERTIES_ERROR.getMessage(), e);
        }
        return studentVOsPage;
    }

    /**
     * 新增
     *
     * @param studentDO
     * @return
     */
    public int insertByAutoInc(StudentDO studentDO) {
        return studentDao.insertByAutoInc(studentDO);
    }

    /**
     * 批量新增
     *
     * @return
     */
    public int batchInsertByAutoInc() {
        List<StudentDO> studentDOs = new ArrayList<>();
        // 注意：在执行批量新增前需要判断list是否为空
        if (CollectionUtils.isEmpty(studentDOs)) {
            return 0;
        }
        return studentDao.batchInsertByAutoInc(studentDOs);
    }

}
