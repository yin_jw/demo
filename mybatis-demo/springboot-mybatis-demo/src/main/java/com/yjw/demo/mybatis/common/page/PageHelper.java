package com.yjw.demo.mybatis.common.page;

/**
 * 分页帮助类
 *
 * @author yinjianwei
 * @date 2018/11/05
 */
@SuppressWarnings("rawtypes")
public class PageHelper {

    private static final ThreadLocal<Page> PAGE_THREADLOCAT = new ThreadLocal<>();

    /**
     * 设置线程局部变量分页信息
     *
     * @param page
     */
    public static void setPageThreadLocal(Page page) {
        PAGE_THREADLOCAT.set(page);
    }

    /**
     * 获取线程局部变量分页信息
     *
     * @return
     */
    public static Page getPageThreadLocal() {
        return PAGE_THREADLOCAT.get();
    }

    /**
     * 清空线程局部变量分页信息
     */
    public static void pageThreadLocalClear() {
        PAGE_THREADLOCAT.remove();
    }

    /**
     * 设置分页参数
     *
     * @param pageNum
     * @param pageSize
     */
    public static void startPage(Integer pageNum, Integer pageSize) {
        Page page = new Page();
        page.setPageNum(pageNum);
        page.setPageSize(pageSize);
        setPageThreadLocal(page);
    }

}
