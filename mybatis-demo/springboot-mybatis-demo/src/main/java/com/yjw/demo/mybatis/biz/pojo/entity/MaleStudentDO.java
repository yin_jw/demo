package com.yjw.demo.mybatis.biz.pojo.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 男生
 * 
 * @author yinjianwei
 * @date 2018/09/30
 */
public class MaleStudentDO extends StudentDO implements Serializable {

    private List<StudentHealthMaleDO> studentHealthMales;

    /**
     * @return the studentHealthMales
     */
    public List<StudentHealthMaleDO> getStudentHealthMales() {
        return studentHealthMales;
    }

    /**
     * @param studentHealthMales the studentHealthMales to set
     */
    public void setStudentHealthMales(List<StudentHealthMaleDO> studentHealthMales) {
        this.studentHealthMales = studentHealthMales;
    }

}
