package com.yjw.demo.mybatis.biz.controller;

import java.util.List;

import com.yjw.demo.mybatis.biz.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.biz.pojo.vo.StudentVO;
import com.yjw.demo.mybatis.common.constant.ErrorCodeEnum;
import com.yjw.demo.mybatis.common.pojo.DataResponse;
import com.yjw.demo.mybatis.common.pojo.PageResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 学生
 * 
 * @author yinjianwei
 * @date 2018/09/27
 */
@Api(tags = {"学生"})
@RestController
@RequestMapping(value = "/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    /**
     * 根据条件筛选试卷数据
     * 
     * @param studentQuery 查询条件
     * @return
     */
    @GetMapping("/list_student_by_conditions")
    @ApiOperation(value = "根据条件筛选试卷数据", httpMethod = "GET")
    @ApiImplicitParams({@ApiImplicitParam(name = "studentQuery", value = "查询条件", required = true, dataType = "object")})
    public DataResponse listStudentByCondition(StudentQuery studentQuery) {
        List<StudentVO> studentVOs = studentService.listStudentByConditions(studentQuery);
        return DataResponse.ok(studentVOs);
    }

    /**
     * 根据条件筛选试卷分页数据
     * 
     * @param studentQuery 查询条件
     * @return
     */
    @GetMapping("/page_student_by_conditions")
    public DataResponse pageStudentByCondition(StudentQuery studentQuery) {
        // 参数验证
        if (studentQuery.getPageNum() == null || studentQuery.getPageSize() == null) {
            DataResponse.response(ErrorCodeEnum.PARAMETER_REQUIRED.getCode(),
                ErrorCodeEnum.PARAMETER_REQUIRED.getMessage());
        }
        PageResult<StudentVO> studentVOsPage = studentService.pageStudentByConditions(studentQuery);
        return DataResponse.ok(studentVOsPage);
    }

}