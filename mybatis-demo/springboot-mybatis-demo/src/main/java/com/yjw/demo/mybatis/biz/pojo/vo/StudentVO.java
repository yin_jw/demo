package com.yjw.demo.mybatis.biz.pojo.vo;

import com.yjw.demo.mybatis.common.constant.Sex;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentVO {
    private Long id;

    private String name;

    private Sex sex;

    private String sexValue;

    private Long selfcardNo;

    private String note;

    private StudentSelfcardVO studentSelfcard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    /**
     * @return the sexValue
     */
    public String getSexValue() {
        return sexValue;
    }

    /**
     * @param sexValue the sexValue to set
     */
    public void setSexValue(String sexValue) {
        this.sexValue = sexValue;
    }

    public Long getSelfcardNo() {
        return selfcardNo;
    }

    public void setSelfcardNo(Long selfcardNo) {
        this.selfcardNo = selfcardNo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * @return the studentSelfcard
     */
    public StudentSelfcardVO getStudentSelfcard() {
        return studentSelfcard;
    }

    /**
     * @param studentSelfcard the studentSelfcard to set
     */
    public void setStudentSelfcard(StudentSelfcardVO studentSelfcard) {
        this.studentSelfcard = studentSelfcard;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StudentVO [id=");
        builder.append(id);
        builder.append(", name=");
        builder.append(name);
        builder.append(", sex=");
        builder.append(sex);
        builder.append(", sexValue=");
        builder.append(sexValue);
        builder.append(", selfcardNo=");
        builder.append(selfcardNo);
        builder.append(", note=");
        builder.append(note);
        builder.append(", studentSelfcard=");
        builder.append(studentSelfcard);
        builder.append("]");
        return builder.toString();
    }

}
