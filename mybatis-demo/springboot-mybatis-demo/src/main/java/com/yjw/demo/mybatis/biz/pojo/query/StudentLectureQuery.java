package com.yjw.demo.mybatis.biz.pojo.query;

import java.util.List;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentLectureQuery {

    private Long id;
    private List<Long> ids;
    private Long studentId;
    private Long lectureId;
    private Float minValue;
    private Float maxValue;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the ids
     */
    public List<Long> getIds() {
        return ids;
    }

    /**
     * @param ids the ids to set
     */
    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    /**
     * @return the studentId
     */
    public Long getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the lectureId
     */
    public Long getLectureId() {
        return lectureId;
    }

    /**
     * @param lectureId the lectureId to set
     */
    public void setLectureId(Long lectureId) {
        this.lectureId = lectureId;
    }

    /**
     * @return the minValue
     */
    public Float getMinValue() {
        return minValue;
    }

    /**
     * @param minValue the minValue to set
     */
    public void setMinValue(Float minValue) {
        this.minValue = minValue;
    }

    /**
     * @return the maxValue
     */
    public Float getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue the maxValue to set
     */
    public void setMaxValue(Float maxValue) {
        this.maxValue = maxValue;
    }

}
