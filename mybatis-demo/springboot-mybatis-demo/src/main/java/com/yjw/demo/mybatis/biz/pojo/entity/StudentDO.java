package com.yjw.demo.mybatis.biz.pojo.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yjw.demo.mybatis.common.constant.Sex;

@JsonIgnoreProperties(value = {"handler"})
public class StudentDO implements Serializable {

    private Long id;

    private String name;

    private Sex sex;

    private Long selfcardNo;

    private String note;

    private StudentSelfcardDO studentSelfcard;

    private List<StudentLectureDO> studentLectures;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Long getSelfcardNo() {
        return selfcardNo;
    }

    public void setSelfcardNo(Long selfcardNo) {
        this.selfcardNo = selfcardNo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * @return the studentSelfcard
     */
    public StudentSelfcardDO getStudentSelfcard() {
        return studentSelfcard;
    }

    /**
     * @param studentSelfcard the studentSelfcard to set
     */
    public void setStudentSelfcard(StudentSelfcardDO studentSelfcard) {
        this.studentSelfcard = studentSelfcard;
    }

    /**
     * @return the studentLectures
     */
    public List<StudentLectureDO> getStudentLectures() {
        return studentLectures;
    }

    /**
     * @param studentLectures the studentLectures to set
     */
    public void setStudentLectures(List<StudentLectureDO> studentLectures) {
        this.studentLectures = studentLectures;
    }

}