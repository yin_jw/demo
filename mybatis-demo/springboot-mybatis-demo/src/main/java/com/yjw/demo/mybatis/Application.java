package com.yjw.demo.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动，配置类
 * 
 * @author yinjianwei
 * @date 2017/12/06
 */
@SpringBootApplication
@MapperScan("com.yjw.demo.mybatis.biz.dao")
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

}
