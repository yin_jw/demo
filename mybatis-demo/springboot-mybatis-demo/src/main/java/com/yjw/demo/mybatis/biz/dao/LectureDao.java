package com.yjw.demo.mybatis.biz.dao;

import java.util.List;

import com.yjw.demo.mybatis.biz.pojo.entity.LectureDO;
import com.yjw.demo.mybatis.biz.pojo.query.LectureQuery;

/**
 * 课程dao
 * 
 * @author yinjianwei
 * @date 2018/09/26
 */
public interface LectureDao {

    /**
     * 根据主键查询课程
     * 
     * @param id
     * @return
     */
    LectureDO getByPrimaryKey(Long id);

    /**
     * 根据条件获取课程信息
     * 
     * @param lectureQuery
     * @return
     */
    List<LectureDO> listByConditions(LectureQuery lectureQuery);

    /**
     * 根据条件计算课程数量
     * 
     * @param lectureQuery
     * @return
     */
    int countByConditions(LectureQuery lectureQuery);

    /**
     * 新增课程信息
     * 
     * @param lectureDO
     * @return
     */
    int insert(LectureDO lectureDO);

    /**
     * 选择性新增课程信息
     * 
     * @param lectureDO
     * @return
     */
    int insertSelective(LectureDO lectureDO);

    /**
     * 根据主键更新课程信息
     * 
     * @param lectureDO
     * @return
     */
    int updateByPrimaryKey(LectureDO lectureDO);

    /**
     * 根据主键选择性更新课程信息
     * 
     * @param lectureDO
     * @return
     */
    int updateByPrimaryKeySelective(LectureDO lectureDO);

    /**
     * 根据主键删除课程数据
     * 
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据条件删除课程数据
     * 
     * @param lectureQuery
     * @return
     */
    int deleteByConditions(LectureQuery lectureQuery);
}