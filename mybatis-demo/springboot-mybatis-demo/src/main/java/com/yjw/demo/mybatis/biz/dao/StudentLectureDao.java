package com.yjw.demo.mybatis.biz.dao;

import java.util.List;

import com.yjw.demo.mybatis.biz.pojo.entity.StudentLectureDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentLectureQuery;

/**
 * 学生课程分数dao
 * 
 * @author yinjianwei
 * @date 2018/09/26
 */
public interface StudentLectureDao {

    /**
     * 根据主键查询学生课程分数
     * 
     * @param id
     * @return
     */
    StudentLectureDO getByPrimaryKey(Long id);

    /**
     * 根据条件获取学生课程分数信息
     * 
     * @param studentLectureQuery
     * @return
     */
    List<StudentLectureDO> listByConditions(StudentLectureQuery studentLectureQuery);

    /**
     * 根据条件计算学生课程分数数量
     * 
     * @param studentLectureQuery
     * @return
     */
    int countByConditions(StudentLectureQuery studentLectureQuery);

    /**
     * 新增学生课程分数信息
     * 
     * @param studentLectureDO
     * @return
     */
    int insert(StudentLectureDO studentLectureDO);

    /**
     * 选择性新增学生课程分数信息
     * 
     * @param studentLectureDO
     * @return
     */
    int insertSelective(StudentLectureDO studentLectureDO);

    /**
     * 根据主键更新学生课程分数信息
     * 
     * @param studentLectureDO
     * @return
     */
    int updateByPrimaryKey(StudentLectureDO studentLectureDO);

    /**
     * 根据主键选择性更新学生课程分数信息
     * 
     * @param studentLectureDO
     * @return
     */
    int updateByPrimaryKeySelective(StudentLectureDO studentLectureDO);

    /**
     * 根据主键删除学生课程分数数据
     * 
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据条件删除学生课程分数数据
     * 
     * @param studentLectureQuery
     * @return
     */
    int deleteByConditions(StudentLectureQuery studentLectureQuery);
}