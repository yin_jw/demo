package com.yjw.demo.mybatis.biz.dao;

import java.util.List;

import com.yjw.demo.mybatis.biz.pojo.entity.StudentHealthFemaleDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentHealthFemaleQuery;

/**
 * 女学生体检dao
 * 
 * @author yinjianwei
 * @date 2018/09/26
 */
public interface StudentHealthFemaleDao {

    /**
     * 根据主键查询女学生体检
     * 
     * @param id
     * @return
     */
    StudentHealthFemaleDO getByPrimaryKey(Long id);

    /**
     * 根据条件获取女学生体检信息
     * 
     * @param studentHealthFemaleQuery
     * @return
     */
    List<StudentHealthFemaleDO> listByConditions(StudentHealthFemaleQuery studentHealthFemaleQuery);

    /**
     * 根据条件计算女学生体检数量
     * 
     * @param studentHealthFemaleQuery
     * @return
     */
    int countByConditions(StudentHealthFemaleQuery studentHealthFemaleQuery);

    /**
     * 新增女学生体检信息
     * 
     * @param studentHealthFemaleDO
     * @return
     */
    int insert(StudentHealthFemaleDO studentHealthFemaleDO);

    /**
     * 选择性新增女学生体检信息
     * 
     * @param studentHealthFemaleDO
     * @return
     */
    int insertSelective(StudentHealthFemaleDO studentHealthFemaleDO);

    /**
     * 根据主键更新女学生体检信息
     * 
     * @param studentHealthFemaleDO
     * @return
     */
    int updateByPrimaryKey(StudentHealthFemaleDO studentHealthFemaleDO);

    /**
     * 根据主键选择性更新女学生体检信息
     * 
     * @param studentHealthFemaleDO
     * @return
     */
    int updateByPrimaryKeySelective(StudentHealthFemaleDO studentHealthFemaleDO);

    /**
     * 根据主键删除女学生体检数据
     * 
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据条件删除女学生体检数据
     * 
     * @param studentHealthFemaleQuery
     * @return
     */
    int deleteByConditions(StudentHealthFemaleQuery studentHealthFemaleQuery);
}