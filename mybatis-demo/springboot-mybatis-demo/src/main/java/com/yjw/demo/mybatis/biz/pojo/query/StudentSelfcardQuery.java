package com.yjw.demo.mybatis.biz.pojo.query;

import java.util.List;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentSelfcardQuery {
    private List<Long> ids;
    private Long studentId;
    private String nativePlace;
    private String idBeginDate;
    private String idEndDate;
    private String edBeginDate;
    private String edEndDate;

    /**
     * @return the ids
     */
    public List<Long> getIds() {
        return ids;
    }

    /**
     * @param ids the ids to set
     */
    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    /**
     * @return the studentId
     */
    public Long getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the nativePlace
     */
    public String getNativePlace() {
        return nativePlace;
    }

    /**
     * @param nativePlace the nativePlace to set
     */
    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    /**
     * @return the idBeginDate
     */
    public String getIdBeginDate() {
        return idBeginDate;
    }

    /**
     * @param idBeginDate the idBeginDate to set
     */
    public void setIdBeginDate(String idBeginDate) {
        this.idBeginDate = idBeginDate;
    }

    /**
     * @return the idEndDate
     */
    public String getIdEndDate() {
        return idEndDate;
    }

    /**
     * @param idEndDate the idEndDate to set
     */
    public void setIdEndDate(String idEndDate) {
        this.idEndDate = idEndDate;
    }

    /**
     * @return the edBeginDate
     */
    public String getEdBeginDate() {
        return edBeginDate;
    }

    /**
     * @param edBeginDate the edBeginDate to set
     */
    public void setEdBeginDate(String edBeginDate) {
        this.edBeginDate = edBeginDate;
    }

    /**
     * @return the edEndDate
     */
    public String getEdEndDate() {
        return edEndDate;
    }

    /**
     * @param edEndDate the edEndDate to set
     */
    public void setEdEndDate(String edEndDate) {
        this.edEndDate = edEndDate;
    }

}
