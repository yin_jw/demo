package com.yjw.demo.mybatis.biz.pojo.query;

import java.util.List;

import com.yjw.demo.mybatis.common.pojo.PageQuery;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentQuery extends PageQuery {

    private List<Long> ids;

    private String name;

    private Byte sex;

    private Long selfcardNo;

    /**
     * @return the ids
     */
    public List<Long> getIds() {
        return ids;
    }

    /**
     * @param ids the ids to set
     */
    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the sex
     */
    public Byte getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(Byte sex) {
        this.sex = sex;
    }

    /**
     * @return the selfcardNo
     */
    public Long getSelfcardNo() {
        return selfcardNo;
    }

    /**
     * @param selfcardNo the selfcardNo to set
     */
    public void setSelfcardNo(Long selfcardNo) {
        this.selfcardNo = selfcardNo;
    }

}
