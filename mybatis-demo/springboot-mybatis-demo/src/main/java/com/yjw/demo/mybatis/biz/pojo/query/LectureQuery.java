package com.yjw.demo.mybatis.biz.pojo.query;

import java.util.List;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class LectureQuery {
    
    private Long id;
    private List<Long> ids;
    private String lectureName;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the ids
     */
    public List<Long> getIds() {
        return ids;
    }

    /**
     * @param ids the ids to set
     */
    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    /**
     * @return the lectureName
     */
    public String getLectureName() {
        return lectureName;
    }

    /**
     * @param lectureName the lectureName to set
     */
    public void setLectureName(String lectureName) {
        this.lectureName = lectureName;
    }

}
