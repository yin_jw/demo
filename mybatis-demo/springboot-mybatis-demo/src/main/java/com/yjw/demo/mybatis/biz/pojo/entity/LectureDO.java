package com.yjw.demo.mybatis.biz.pojo.entity;

import java.io.Serializable;

public class LectureDO implements Serializable {

    private Long id;

    private String lectureName;

    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = lectureName == null ? null : lectureName.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }
}