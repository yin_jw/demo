package com.yjw.demo.mybatis.biz.pojo.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 女生
 * 
 * @author yinjianwei
 * @date 2018/09/30
 */
public class FemaleStudentDO extends StudentDO implements Serializable {

    private List<StudentHealthFemaleDO> studentHealthFemales;

    /**
     * @return the studentHealthFemales
     */
    public List<StudentHealthFemaleDO> getStudentHealthFemales() {
        return studentHealthFemales;
    }

    /**
     * @param studentHealthFemales the studentHealthFemales to set
     */
    public void setStudentHealthFemales(List<StudentHealthFemaleDO> studentHealthFemales) {
        this.studentHealthFemales = studentHealthFemales;
    }

}
