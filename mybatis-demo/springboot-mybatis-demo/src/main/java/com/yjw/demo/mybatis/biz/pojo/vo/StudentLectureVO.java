package com.yjw.demo.mybatis.biz.pojo.vo;

import java.math.BigDecimal;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentLectureVO {
    private Long id;

    private Long studentId;

    private Long lectureId;

    private BigDecimal grade;

    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getLectureId() {
        return lectureId;
    }

    public void setLectureId(Long lectureId) {
        this.lectureId = lectureId;
    }

    public BigDecimal getGrade() {
        return grade;
    }

    public void setGrade(BigDecimal grade) {
        this.grade = grade;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StudentLectureVO [id=");
        builder.append(id);
        builder.append(", studentId=");
        builder.append(studentId);
        builder.append(", lectureId=");
        builder.append(lectureId);
        builder.append(", grade=");
        builder.append(grade);
        builder.append(", note=");
        builder.append(note);
        builder.append("]");
        return builder.toString();
    }

}
