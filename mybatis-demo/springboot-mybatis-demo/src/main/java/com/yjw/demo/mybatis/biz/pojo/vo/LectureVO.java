package com.yjw.demo.mybatis.biz.pojo.vo;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class LectureVO {
    private Long id;

    private String lectureName;

    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = lectureName == null ? null : lectureName.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LectureVO [id=");
        builder.append(id);
        builder.append(", lectureName=");
        builder.append(lectureName);
        builder.append(", note=");
        builder.append(note);
        builder.append("]");
        return builder.toString();
    }

}
