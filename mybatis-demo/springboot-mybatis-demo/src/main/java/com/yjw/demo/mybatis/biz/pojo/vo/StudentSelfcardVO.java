package com.yjw.demo.mybatis.biz.pojo.vo;

import java.util.Date;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentSelfcardVO {
    private Long id;

    private Long studentId;

    private String nativePlace;

    private Date issueDate;

    private Date endDate;

    private String note;

    private Byte studentEffective;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace == null ? null : nativePlace.trim();
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public Byte getStudentEffective() {
        return studentEffective;
    }

    public void setStudentEffective(Byte studentEffective) {
        this.studentEffective = studentEffective;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StudentSelfcardVO [id=");
        builder.append(id);
        builder.append(", studentId=");
        builder.append(studentId);
        builder.append(", nativePlace=");
        builder.append(nativePlace);
        builder.append(", issueDate=");
        builder.append(issueDate);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", note=");
        builder.append(note);
        builder.append(", studentEffective=");
        builder.append(studentEffective);
        builder.append("]");
        return builder.toString();
    }

}
