package com.yjw.demo.mybatis.common.pojo;

import com.yjw.demo.mybatis.common.constant.ErrorCodeEnum;

/**
 * 响应信息
 * 
 * @author yinjianwei
 * @date 2017/12/13
 */
public class DataResponse extends BaseResponse {
    /**
     * 数据
     */
    private Object data;

    /**
     * 无参构造
     */
    public DataResponse() {
        super();
    }

    /**
     * 构造函数
     * 
     * @param code 编码
     * @param message 信息
     * @param data 数据
     */
    public DataResponse(Integer code, String message, Object data) {
        super(code, message);
        this.data = data;
    }

    /**
     * 返回结果
     * 
     * @param code 编码
     * @return
     */
    public static DataResponse response(Integer code) {
        return new DataResponse(code, "", null);
    }

    /**
     * 返回结果
     * 
     * @param code 编码
     * @param message 信息
     * @return
     */
    public static DataResponse response(Integer code, String message) {
        return new DataResponse(code, message, null);
    }

    /**
     * 返回结果
     * 
     * @param code 编码
     * @param message 信息
     * @param data 数据
     * @return
     */
    public static DataResponse response(Integer code, String message, Object data) {
        return new DataResponse(code, message, data);
    }

    /**
     * 返回系统错误结果
     * 
     * @return
     */
    public static DataResponse error() {
        return new DataResponse(ErrorCodeEnum.SYSTEM_ERROR.getCode(), ErrorCodeEnum.SYSTEM_ERROR.getMessage(), null);
    }

    /**
     * 返回系统错误结果
     * 
     * @param message 信息
     * @return
     */
    public static DataResponse error(String message) {
        return new DataResponse(ErrorCodeEnum.SYSTEM_ERROR.getCode(), message, null);
    }

    /**
     * 返回系统错误结果
     * 
     * @param code 编码
     * @param message 信息
     * @return
     */
    public static DataResponse error(Integer code, String message) {
        return new DataResponse(code, message, null);
    }

    /**
     * 返回系统错误结果
     * 
     * @param message 信息
     * @param data 数据
     * @return
     */
    public static DataResponse error(String message, Object data) {
        return new DataResponse(ErrorCodeEnum.SYSTEM_ERROR.getCode(), message, data);
    }

    /**
     * 返回操作成功结果
     * 
     * @return
     */
    public static DataResponse ok() {
        return new DataResponse(ErrorCodeEnum.OK.getCode(), ErrorCodeEnum.OK.getMessage(), null);
    }

    /**
     * 返回操作成功结果
     * 
     * @param data 数据
     * @return
     */
    public static DataResponse ok(Object data) {
        return new DataResponse(ErrorCodeEnum.OK.getCode(), ErrorCodeEnum.OK.getMessage(), data);
    }

    /**
     * 返回操作成功结果
     * 
     * @param message 信息
     * @param data 数据
     * @return
     */
    public static DataResponse ok(String message, Object data) {
        return new DataResponse(ErrorCodeEnum.OK.getCode(), message, data);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
