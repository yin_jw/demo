package com.yjw.demo.mybatis.common.pojo;

/**
 * 响应信息
 * 
 * @author yinjianwei
 * @date 2018/07/30
 */
public class BaseResponse {
    /**
     * 编码
     */
    private Integer code;
    /**
     * 信息
     */
    private String message;

    public BaseResponse() {
        super();
    }

    public BaseResponse(Integer code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
