package com.yjw.demo.mybatis.common.exception;

/**
 * 业务异常类
 * 
 * @author yinjianwei
 * @date 2017/12/13
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 异常编码
     */
    private Integer errorCode;

    /**
     * 构造函数
     */
    public ServiceException() {
        super();
    }

    /**
     * 构造函数
     * 
     * @param errorCode 异常编码
     */
    public ServiceException(Integer errorCode) {
        super();
        this.errorCode = errorCode;
    }

    /**
     * 构造函数
     * 
     * @param errorCode 异常编码
     * @param message 异常信息
     */
    public ServiceException(Integer errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * 构造函数
     * 
     * @param errorCode 异常编码
     * @param message 异常信息
     * @param cause 异常栈
     */
    public ServiceException(Integer errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

}
