package com.yjw.demo.mybatis.biz.pojo.vo;

import java.util.Date;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentHealthMaleVO {
    private Long id;

    private Long studentId;

    private Date checkDate;

    private String heart;

    private String liver;

    private String spleen;

    private String lung;

    private String kidney;

    private String prostate;

    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getHeart() {
        return heart;
    }

    public void setHeart(String heart) {
        this.heart = heart == null ? null : heart.trim();
    }

    public String getLiver() {
        return liver;
    }

    public void setLiver(String liver) {
        this.liver = liver == null ? null : liver.trim();
    }

    public String getSpleen() {
        return spleen;
    }

    public void setSpleen(String spleen) {
        this.spleen = spleen == null ? null : spleen.trim();
    }

    public String getLung() {
        return lung;
    }

    public void setLung(String lung) {
        this.lung = lung == null ? null : lung.trim();
    }

    public String getKidney() {
        return kidney;
    }

    public void setKidney(String kidney) {
        this.kidney = kidney == null ? null : kidney.trim();
    }

    public String getProstate() {
        return prostate;
    }

    public void setProstate(String prostate) {
        this.prostate = prostate == null ? null : prostate.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StudentHealthMaleVO [id=");
        builder.append(id);
        builder.append(", studentId=");
        builder.append(studentId);
        builder.append(", checkDate=");
        builder.append(checkDate);
        builder.append(", heart=");
        builder.append(heart);
        builder.append(", liver=");
        builder.append(liver);
        builder.append(", spleen=");
        builder.append(spleen);
        builder.append(", lung=");
        builder.append(lung);
        builder.append(", kidney=");
        builder.append(kidney);
        builder.append(", prostate=");
        builder.append(prostate);
        builder.append(", note=");
        builder.append(note);
        builder.append("]");
        return builder.toString();
    }

}
