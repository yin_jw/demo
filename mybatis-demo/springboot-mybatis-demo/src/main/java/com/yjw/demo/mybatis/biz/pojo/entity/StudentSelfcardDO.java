package com.yjw.demo.mybatis.biz.pojo.entity;

import java.io.Serializable;
import java.util.Date;

public class StudentSelfcardDO implements Serializable {

    private Long id;

    private Long studentId;

    private String nativePlace;

    private Date issueDate;

    private Date endDate;

    private String note;

    private Byte studentEffective;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace == null ? null : nativePlace.trim();
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public Byte getStudentEffective() {
        return studentEffective;
    }

    public void setStudentEffective(Byte studentEffective) {
        this.studentEffective = studentEffective;
    }
}