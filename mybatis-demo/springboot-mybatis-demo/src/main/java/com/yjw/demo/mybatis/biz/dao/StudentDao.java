package com.yjw.demo.mybatis.biz.dao;

import com.yjw.demo.mybatis.biz.pojo.entity.StudentDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.common.constant.Sex;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 学生dao
 * 
 * @author yinjianwei
 * @date 2018/09/26
 */
public interface StudentDao {

    /**
     * 根据主键查询学生
     * 
     * @param id
     * @return
     */
    StudentDO getByPrimaryKey(Long id);

    /**
     * 根据条件获取学生信息
     * 
     * @param studentQuery
     * @return
     */
    List<StudentDO> listByConditions(StudentQuery studentQuery);

    /**
     * 使用 Map 传递参数
     *
     * @param params
     * @return
     */
    List<StudentDO> listByMap(Map<String, String> params);

    /**
     * 使用注解方式传递参数
     *
     * @param name
     * @param sex
     * @return
     */
    List<StudentDO> listByParamAnnotation(@Param("name") String name,
                                          @Param("sex") Sex sex);

    /**
     * 根据条件计算学生数量
     * 
     * @param studentQuery
     * @return

    int countByConditions(StudentQuery studentQuery);*/

    /**
     * 新增学生信息-存在自增主键
     * 
     * @param studentDO
     * @return
     */
    int insertByAutoInc(StudentDO studentDO);

    /**
     * 新增学生信息-不存在自增主键
     *
     * @param studentDO
     * @return
     */
    int insertByNoAutoInc(StudentDO studentDO);

    /**
     * 选择性新增学生信息
     * 
     * @param studentDO
     * @return
     */
    int insertSelective(StudentDO studentDO);

    /**
     * 批量新增：存在自增主键
     * 
     * @param studentDOs
     * @return
     */
    int batchInsertByAutoInc(List<StudentDO> studentDOs);

    /**
     * 批量新增：不存在自增主键
     *
     * @param studentDOs
     * @return
     */
    int batchInsertByNoAutoInc(List<StudentDO> studentDOs);

    /**
     * 根据主键更新学生信息
     * 
     * @param studentDO
     * @return
     */
    int updateByPrimaryKey(StudentDO studentDO);

    /**
     * 根据主键选择性更新学生信息
     * 
     * @param studentDO
     * @return
     */
    int updateByPrimaryKeySelective(StudentDO studentDO);

    /**
     * 根据主键删除学生数据
     * 
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据条件删除学生数据
     * 
     * @param studentQuery
     * @return
     */
    int deleteByConditions(StudentQuery studentQuery);

    /**
     * 联合查询：嵌套查询
     *
     * @return
     */
    List<StudentDO> listStudentByNestingQuery();

    /**
     * 联合查询：嵌套结果
     *
     * @return
     */
    List<StudentDO> listStudentByNestingResult();
}