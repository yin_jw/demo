package com.yjw.demo.mybatis.biz.dao;

import java.util.List;

import com.yjw.demo.mybatis.biz.pojo.entity.StudentSelfcardDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentSelfcardQuery;

/**
 * 学生证件dao
 * 
 * @author yinjianwei
 * @date 2018/09/26
 */
public interface StudentSelfcardDao {

    /**
     * 根据主键查询学生证件
     * 
     * @param id
     * @return
     */
    StudentSelfcardDO getByPrimaryKey(Long id);

    /**
     * 根据条件获取学生证件信息
     * 
     * @param studentQuery
     * @return
     */
    List<StudentSelfcardDO> listByConditions(StudentSelfcardQuery studentQuery);

    /**
     * 根据条件计算学生证件数量
     * 
     * @param studentQuery
     * @return
     */
    int countByConditions(StudentSelfcardQuery studentQuery);

    /**
     * 新增学生证件信息
     * 
     * @param studentSelfcardDO
     * @return
     */
    int insert(StudentSelfcardDO studentSelfcardDO);

    /**
     * 选择性新增学生证件信息
     * 
     * @param studentSelfcardDO
     * @return
     */
    int insertSelective(StudentSelfcardDO studentSelfcardDO);

    /**
     * 根据主键更新学生证件信息
     * 
     * @param studentSelfcardDO
     * @return
     */
    int updateByPrimaryKey(StudentSelfcardDO studentSelfcardDO);

    /**
     * 根据主键选择性更新学生证件信息
     * 
     * @param studentSelfcardDO
     * @return
     */
    int updateByPrimaryKeySelective(StudentSelfcardDO studentSelfcardDO);

    /**
     * 根据主键删除学生证件数据
     * 
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据条件删除学生证件数据
     * 
     * @param studentQuery
     * @return
     */
    int deleteByConditions(StudentSelfcardQuery studentQuery);
}