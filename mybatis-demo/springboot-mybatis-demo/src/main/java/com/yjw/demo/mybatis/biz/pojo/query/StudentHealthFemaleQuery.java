 package com.yjw.demo.mybatis.biz.pojo.query;

import java.util.List;

/**
 * @author yinjianwei
 * @date 2018/09/26
 */
public class StudentHealthFemaleQuery {

    private Long id;
    private List<Long> ids;
    private Long studentId;
    private String beginDate;
    private String endDate;
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * @return the ids
     */
    public List<Long> getIds() {
        return ids;
    }
    /**
     * @param ids the ids to set
     */
    public void setIds(List<Long> ids) {
        this.ids = ids;
    }
    /**
     * @return the studentId
     */
    public Long getStudentId() {
        return studentId;
    }
    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }
    /**
     * @return the beginDate
     */
    public String getBeginDate() {
        return beginDate;
    }
    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }
    
    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }
    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    
}
