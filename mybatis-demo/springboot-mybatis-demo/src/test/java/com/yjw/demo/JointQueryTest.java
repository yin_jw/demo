package com.yjw.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.dao.StudentDao;
import com.yjw.demo.mybatis.biz.pojo.entity.StudentDO;
import com.yjw.demo.mybatis.common.util.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 联合查询试类
 *
 * @author yinjianwei
 * @date 2018/09/27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class JointQueryTest {

    @Autowired
    private StudentDao studentDao;

    /**
     * 联合查询-嵌套查询（一对一、一对多、鉴别器）
     *
     * @throws JsonProcessingException
     */
    @Test
    public void listStudentByNestingQuery() throws JsonProcessingException, InterruptedException {
        List<StudentDO> students = studentDao.listStudentByNestingQuery();
        // 1.测试延迟加载的效果
        // Thread.sleep(3000L);
        // System.out.println("睡眠3秒钟");
        // students.get(0).getStudentSelfcard();

        // 2.使用JSON功能转JSON字符串会导致N+1的问题
        System.out.println(JsonUtils.toJSONString(students));
    }

    /**
     * 联合查询-嵌套结果（一对一、一对多、鉴别器）
     *
     * @throws JsonProcessingException
     */
    @Test
    public void listStudentByNestingResult() throws JsonProcessingException {
        List<StudentDO> students = studentDao.listStudentByNestingResult();
        System.out.println(JsonUtils.toJSONString(students));
    }
}