package com.yjw.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.dao.StudentDao;
import com.yjw.demo.mybatis.biz.pojo.entity.StudentDO;
import com.yjw.demo.mybatis.common.constant.Sex;
import com.yjw.demo.mybatis.common.util.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 主键回填测试类
 *
 * @author yinjianwei
 * @date 2018/09/27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class PrimaryKeyTest {

    @Autowired
    private StudentDao studentDao;

    @Test
    public void insertByAutoInc() throws JsonProcessingException {
        StudentDO studentDO = new StudentDO();
        studentDO.setName("a-" + System.currentTimeMillis());
        studentDO.setSex(Sex.MALE);
        studentDO.setSelfcardNo(new Random(10000L).nextLong());
        studentDO.setNote("a-" + System.currentTimeMillis());
        studentDao.insertByAutoInc(studentDO);
        System.out.println(JsonUtils.toJSONString(studentDO));
    }

    @Test
    public void insertByNoAutoInc() throws JsonProcessingException {
        StudentDO studentDO = new StudentDO();
        studentDO.setName("b-" + System.currentTimeMillis());
        studentDO.setSex(Sex.FEMALE);
        studentDO.setSelfcardNo(new Random(10000L).nextLong());
        studentDO.setNote("b-" + System.currentTimeMillis());
        studentDao.insertByNoAutoInc(studentDO);
        System.out.println(JsonUtils.toJSONString(studentDO));
    }

    @Test
    public void batchInsertByAutoInc() throws JsonProcessingException {
        List<StudentDO> studentDOS = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            StudentDO studentDO = new StudentDO();
            studentDO.setName("c-" + i + System.currentTimeMillis());
            studentDO.setSex(Sex.MALE);
            studentDO.setSelfcardNo(new Random(10000L).nextLong());
            studentDO.setNote("c-" + i + System.currentTimeMillis());
            studentDOS.add(studentDO);
        }
        studentDao.batchInsertByAutoInc(studentDOS);
        System.out.println(JsonUtils.toJSONString(studentDOS));
    }

    @Test
    public void batchInsertByNoAutoInc() throws JsonProcessingException {
        List<StudentDO> studentDOS = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            StudentDO studentDO = new StudentDO();
            studentDO.setName("d-" + i + System.currentTimeMillis());
            studentDO.setSex(Sex.FEMALE);
            studentDO.setSelfcardNo(new Random(10000L).nextLong());
            studentDO.setNote("d-" + i + System.currentTimeMillis());
            studentDOS.add(studentDO);
        }
        studentDao.batchInsertByAutoInc(studentDOS);
        System.out.println(JsonUtils.toJSONString(studentDOS));
    }
}