package com.yjw.demo;

import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.pojo.entity.StudentDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.common.constant.Sex;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 缓存测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class CacheTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheTest.class);

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    /**
     * 一级缓存
     */
    @Test
    public void l1Cache() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        long startTime1 = System.currentTimeMillis();
        sqlSession.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.listByConditions");
        LOGGER.info("第一次查询执行时间：" + (System.currentTimeMillis() - startTime1));

        long startTime2 = System.currentTimeMillis();
        sqlSession.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.listByConditions");
        LOGGER.info("第二次查询执行时间：" + (System.currentTimeMillis() - startTime2));
        sqlSession.close();
    }

    /**
     * 二级缓存
     */
    @Test
    public void l2Cache() {
        SqlSession sqlSession1 = sqlSessionFactory.openSession();
        long startTime1 = System.currentTimeMillis();
        sqlSession1.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.listByConditions",
                new StudentQuery());
        LOGGER.info("第一个SqlSession查询执行时间：" + (System.currentTimeMillis() - startTime1));
        sqlSession1.commit();
        sqlSession1.close();

        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        long startTime2 = System.currentTimeMillis();
        sqlSession2.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.listByConditions",
                new StudentQuery());
        LOGGER.info("第二个SqlSession查询执行时间：" + (System.currentTimeMillis() - startTime2));
        sqlSession2.commit();
        sqlSession2.close();
    }

    /**
     * 测试二级缓存全部失效问题，只要执行了insert、update、delete
     * 就会刷新同一个 namespace 下的所有缓存数据
     */
    @Test
    public void l2CacheInvalid() {
        // 缓存listByConditions的数据
        SqlSession sqlSession1 = sqlSessionFactory.openSession();
        long startTime1 = System.currentTimeMillis();
        sqlSession1.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.listByConditions",
                new StudentQuery());
        LOGGER.info("第一个SqlSession查询执行时间：" + (System.currentTimeMillis() - startTime1));
        sqlSession1.commit();
        sqlSession1.close();

        // 缓存getByPrimaryKey的数据
        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        long startTime2 = System.currentTimeMillis();
        sqlSession2.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.getByPrimaryKey",
                1L);
        LOGGER.info("第二个SqlSession查询执行时间：" + (System.currentTimeMillis() - startTime2));
        sqlSession2.commit();
        sqlSession2.close();

        // 执行insert语句使上面所有缓存失效
        SqlSession sqlSession3 = sqlSessionFactory.openSession();
        StudentDO studentDO = new StudentDO();
        studentDO.setName("赵六");
        studentDO.setSex(Sex.MALE);
        studentDO.setSelfcardNo(4444L);
        studentDO.setNote("zhaoliu");
        sqlSession3.insert("com.yjw.demo.mybatis.biz.dao.StudentDao.insertByAutoInc", studentDO);
        sqlSession3.commit();
        sqlSession3.close();

        // 再次执行上面缓存的数据，查看缓存是否已经失效
        SqlSession sqlSession4 = sqlSessionFactory.openSession();
        long startTime4 = System.currentTimeMillis();
        sqlSession4.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.listByConditions",
                new StudentQuery());
        LOGGER.info("第四个SqlSession查询执行时间：" + (System.currentTimeMillis() - startTime4));
        sqlSession4.commit();
        sqlSession4.close();

        // 缓存getByPrimaryKey的数据
        SqlSession sqlSession5 = sqlSessionFactory.openSession();
        long startTime5 = System.currentTimeMillis();
        sqlSession5.selectList("com.yjw.demo.mybatis.biz.dao.StudentDao.getByPrimaryKey",
                1L);
        LOGGER.info("第五个SqlSession查询执行时间：" + (System.currentTimeMillis() - startTime5));
        sqlSession5.commit();
        sqlSession5.close();
    }
}