package com.yjw.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.dao.StudentDao;
import com.yjw.demo.mybatis.biz.pojo.entity.StudentDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.common.constant.Sex;
import com.yjw.demo.mybatis.common.util.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 传递多个参数测试类
 *
 * @author yinjianwei
 * @date 2018/09/27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MulParametersTest {

    @Autowired
    private StudentDao studentDao;

    @Test
    public void listByMap() throws JsonProcessingException {
        Map<String, String> params = new HashMap<>();
        params.put("name", "张三");
        List<StudentDO> students = studentDao.listByMap(params);
        System.out.println(JsonUtils.toJSONString(students));
    }

    @Test
    public void listByParamAnnotation() throws JsonProcessingException {
        List<StudentDO> students = studentDao.listByParamAnnotation("张", Sex.MALE);
        System.out.println(JsonUtils.toJSONString(students));
    }

    @Test
    public void listByConditions() throws JsonProcessingException {
        StudentQuery studentQuery = new StudentQuery();
        studentQuery.setName("张");
        List<StudentDO> students = studentDao.listByConditions(studentQuery);
        System.out.println(JsonUtils.toJSONString(students));
    }
}