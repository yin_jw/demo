package com.yjw.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.biz.pojo.vo.StudentVO;
import com.yjw.demo.mybatis.biz.service.StudentService;
import com.yjw.demo.mybatis.common.pojo.PageResult;
import com.yjw.demo.mybatis.common.util.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 分页插件测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class PageTest {

    @Autowired
    private StudentService studentService;

    @Test
    public void pageStudentByConditions() throws JsonProcessingException {
        StudentQuery studentQuery = new StudentQuery();
        studentQuery.setPageNum(1);
        studentQuery.setPageSize(10);
        PageResult<StudentVO> students =  studentService.pageStudentByConditions(studentQuery);
        System.out.println(JsonUtils.toJSONString(students));
    }
}