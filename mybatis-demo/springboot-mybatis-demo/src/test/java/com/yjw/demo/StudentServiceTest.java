package com.yjw.demo;

import java.util.List;

import com.yjw.demo.mybatis.biz.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.biz.pojo.vo.StudentVO;
import com.yjw.demo.mybatis.common.pojo.PageResult;
import com.yjw.demo.mybatis.common.util.JsonUtils;

/**
 * StudentService 测试类
 * 
 * @author yinjianwei
 * @date 2018/09/27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class StudentServiceTest {

    @Autowired
    private StudentService studentService;

    @Test
    public void listStudentByConditions() throws JsonProcessingException {
        StudentQuery studentQuery = new StudentQuery();
        studentQuery.setSex((byte)1);
        List<StudentVO> studentVOs = studentService.listStudentByConditions(studentQuery);
        System.out.println(JsonUtils.toJSONString(studentVOs));
    }

    @Test
    public void pageStudentByConditions() throws JsonProcessingException {
        StudentQuery studentQuery = new StudentQuery();
        studentQuery.setSex((byte)1);
        studentQuery.setPageNum(1);
        studentQuery.setPageSize(10);
        PageResult<StudentVO> studentVOsPage = studentService.pageStudentByConditions(studentQuery);
        System.out.println(JsonUtils.toJSONString(studentVOsPage));
    }
}