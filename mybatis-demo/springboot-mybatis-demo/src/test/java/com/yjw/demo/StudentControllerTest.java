package com.yjw.demo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.controller.StudentController;

/**
 * 学生测试类
 * 
 * @author yinjianwei
 * @date 2018/09/27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class StudentControllerTest {

    @Autowired
    private StudentController studentController;

    private MockMvc mockMvc;

    @Before
    public void befor() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();
    }

    @Test
    public void listStudentByCondition() throws Exception {
        MvcResult mvcResult = mockMvc
            .perform(MockMvcRequestBuilders.get("/student/list_student_by_conditions")
                .accept(MediaType.APPLICATION_JSON).param("name", "二"))
            .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
        System.out.println("***************************");
        System.out.println("输出：" + mvcResult.getResponse().getContentAsString());
        System.out.println("***************************");
    }
}
