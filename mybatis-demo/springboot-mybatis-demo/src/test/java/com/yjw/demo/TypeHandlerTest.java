package com.yjw.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yjw.demo.mybatis.Application;
import com.yjw.demo.mybatis.biz.dao.StudentDao;
import com.yjw.demo.mybatis.biz.pojo.entity.StudentDO;
import com.yjw.demo.mybatis.biz.pojo.query.StudentQuery;
import com.yjw.demo.mybatis.biz.pojo.vo.StudentVO;
import com.yjw.demo.mybatis.biz.service.StudentService;
import com.yjw.demo.mybatis.common.constant.Sex;
import com.yjw.demo.mybatis.common.util.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 类型处理器测试类
 *
 * @author yinjianwei
 * @date 2018/09/27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TypeHandlerTest {

    @Autowired
    private StudentDao studentDao;

    @Test
    public void insert() {
        StudentDO studentDO = new StudentDO();
        studentDO.setName("李四");
        studentDO.setSex(Sex.MALE);
        studentDO.setSelfcardNo(222L);
        studentDO.setNote("lisi");
        studentDao.insertByAutoInc(studentDO);
    }

    @Test
    public void listByConditions() throws JsonProcessingException {
        List<StudentDO> students = studentDao.listByConditions(new StudentQuery());
        System.out.println(JsonUtils.toJSONString(students));
    }

}