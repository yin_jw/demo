/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.17 : Database - mybatis-demo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mybatis-demo` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mybatis-demo`;

/*Table structure for table `t_lecture` */

DROP TABLE IF EXISTS `t_lecture`;

CREATE TABLE `t_lecture` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `lecture_name` varchar(60) NOT NULL COMMENT '课程名称',
  `note` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程表';

/*Table structure for table `t_student` */

DROP TABLE IF EXISTS `t_student`;

CREATE TABLE `t_student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(60) NOT NULL COMMENT '学生名称',
  `sex` tinyint(4) NOT NULL COMMENT '性别',
  `selfcard_no` bigint(20) NOT NULL COMMENT '学生证号',
  `note` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='学生表';

/*Table structure for table `t_student_health_female` */

DROP TABLE IF EXISTS `t_student_health_female`;

CREATE TABLE `t_student_health_female` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `student_id` bigint(20) NOT NULL COMMENT '学生主键',
  `check_date` date NOT NULL COMMENT '检查日期',
  `heart` varchar(60) NOT NULL COMMENT '心',
  `liver` varchar(60) NOT NULL COMMENT '肝',
  `spleen` varchar(60) NOT NULL COMMENT '脾',
  `lung` varchar(60) NOT NULL COMMENT '肺',
  `kidney` varchar(60) NOT NULL COMMENT '肾',
  `uterus` varchar(60) NOT NULL COMMENT '子宫',
  `note` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='女学生体检表';

/*Table structure for table `t_student_health_male` */

DROP TABLE IF EXISTS `t_student_health_male`;

CREATE TABLE `t_student_health_male` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `student_id` bigint(20) NOT NULL COMMENT '学生主键',
  `check_date` date NOT NULL COMMENT '检查日期',
  `heart` varchar(60) NOT NULL COMMENT '心',
  `liver` varchar(60) NOT NULL COMMENT '肝',
  `spleen` varchar(60) NOT NULL COMMENT '脾',
  `lung` varchar(60) NOT NULL COMMENT '肺',
  `kidney` varchar(60) NOT NULL COMMENT '肾',
  `prostate` varchar(60) NOT NULL COMMENT '前列腺',
  `note` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='男学生体检表';

/*Table structure for table `t_student_lecture` */

DROP TABLE IF EXISTS `t_student_lecture`;

CREATE TABLE `t_student_lecture` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `student_id` bigint(20) NOT NULL COMMENT '学生主键',
  `lecture_id` bigint(20) NOT NULL COMMENT '课程主键',
  `grade` decimal(16,2) NOT NULL COMMENT '评分',
  `note` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生课程成绩表';

/*Table structure for table `t_student_selfcard` */

DROP TABLE IF EXISTS `t_student_selfcard`;

CREATE TABLE `t_student_selfcard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `student_id` bigint(20) NOT NULL COMMENT '学生主键',
  `native_place` varchar(60) NOT NULL COMMENT '籍贯',
  `issue_date` date NOT NULL COMMENT '发证日期',
  `end_date` date NOT NULL COMMENT '结束发证日期',
  `note` varchar(1024) DEFAULT NULL COMMENT '备注',
  `student_effective` tinyint(4) NOT NULL COMMENT '学生是否有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生证件表';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
