# mybatis-demo

#### 项目介绍
MyBatis 使用实例，帮助大家快速使用 MyBatis 框架

#### 软件架构
Spring Boot

Druid

MyBatis

Mybatis-PageHelper

Swagger

#### 参考

1. MyBatis官网文档：[http://www.mybatis.org/mybatis-3/zh/index.html ](http://www.mybatis.org/mybatis-3/zh/index.html)，遇到疑问参考一下 MyBatis 的官网，里面有对各种配置文件的属性描述。

#### 使用说明

1. 在数据库中建表，建表语句在的 db 目录
2. 修改 application.yml 配置，通过 Application.java 类启动项目

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)