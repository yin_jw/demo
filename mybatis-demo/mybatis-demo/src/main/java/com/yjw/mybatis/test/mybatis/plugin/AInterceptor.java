package com.yjw.mybatis.test.mybatis.plugin;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

@Intercepts({
        @Signature(type = Executor.class, method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class,
                ResultHandler.class, CacheKey.class, BoundSql.class})})
public class AInterceptor implements Interceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AInterceptor.class);

    /**
     * 执行拦截逻辑的方法
     *
     * @param invocation
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        LOGGER.info("--------------执行拦截器A前--------------");
        Object obj = invocation.proceed();
        LOGGER.info("--------------执行拦截器A后--------------");
        return obj;
    }

    /**
     * 决定是否触发intercept()方法
     *
     * @param target
     * @return
     */
    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    /**
     * 根据配置初始化Interceptor对象
     *
     * @param properties
     */
    @Override
    public void setProperties(Properties properties) {

    }
}
