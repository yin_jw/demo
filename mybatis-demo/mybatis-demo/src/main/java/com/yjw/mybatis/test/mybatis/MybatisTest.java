package com.yjw.mybatis.test.mybatis;

import com.yjw.mybatis.dao.StudentMapper;
import com.yjw.mybatis.entity.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * mybatis 测试例子
 * 
 * @author yinjianwei
 * @date 2019/01/24
 */
public class MybatisTest {

    @Test
    public void selectByPrimaryKey() throws IOException {
        StudentMapper studentMapper = getSqlSession().getMapper(StudentMapper.class);
        Student student = studentMapper.selectByPrimaryKey(1L);
        System.out.println(student);
    }

    /**
     * 测试字段为空的情况
     *
     * @throws IOException
     */
    @Test
    public void insert() throws IOException {
        StudentMapper studentMapper = getSqlSession().getMapper(StudentMapper.class);
        Student student = new Student();
        student.setSex(new Byte("1"));
        student.setSelfcardNo(123456789L);
        studentMapper.insert(student);
        System.out.println(student);
    }

    /**
     * 获取SqlSession
     * 
     * @return
     */
    private SqlSession getSqlSession() throws IOException {
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        // 加载配置文件得到SqlSessionFactory
        return sqlSessionFactory.openSession(true);
    }
}
