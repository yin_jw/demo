package com.yjw.mybatis.test.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

/**
 * jdbc 测试例子
 * 
 * @author yinjianwei
 * @date 2019/01/24
 */
public class JdbcTest {

    /**
     * 新增
     */
    @Test
    public void insert() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement("INSERT INTO t_student (NAME, sex, selfcard_no, note) VALUES (?, ?, ?, ?)");
            ps.setString(1, "王五");
            ps.setInt(2, 1);
            ps.setLong(3, 111111L);
            ps.setString(4, "wangwu");
            int count = ps.executeUpdate();
            conn.commit();
            System.out.println("新增" + count + "条数据");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != ps) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (null != conn) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 删除
     */
    @Test
    public void delete() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement("DELETE FROM t_student WHERE id = ?");
            ps.setInt(1, 3);
            int count = ps.executeUpdate();
            conn.commit();
            System.out.println("删除" + count + "条数据");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != ps) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (null != conn) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 更新
     */
    @Test
    public void update() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement("UPDATE t_student SET NAME = ? WHERE id = ?");
            ps.setString(1, "王五2");
            ps.setInt(2, 4);
            int count = ps.executeUpdate();
            conn.commit();
            System.out.println("更新" + count + "条数据");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != ps) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (null != conn) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 查询
     */
    @Test
    public void selectList() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement("SELECT id, NAME, sex, selfcard_no, note FROM t_student");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                long id = rs.getLong(1);
                String name = rs.getString(2);
                int sex = rs.getInt(3);
                long selfcardNo = rs.getLong(4);
                String note = rs.getString(5);
                System.out.println(
                    "id：" + id + "，name：" + name + "，sex：" + sex + "，selfcardNo" + selfcardNo + "，note" + note);
            }
            conn.commit();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != ps) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (null != conn) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获得一个连接
     * 
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection conn = null;
        //Class.forName("com.mysql.cj.jdbc.Driver");
        conn = DriverManager.getConnection(
            "jdbc:mysql://localhost:3306/mybatis-demo?useUnicode=true&characterEncoding=utf-8&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
            "root", "123456");
        return conn;
    }

}
