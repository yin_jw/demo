package com.yjw.rpc;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        RpcProxyClient rpcProxyClient = new RpcProxyClient();
        IHelloService helloService = rpcProxyClient.clientProxy(IHelloService.class, "localhost", 9090);
        String result = helloService.sayHello("james");
        System.out.println(result);
    }
}