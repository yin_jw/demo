package com.yjw.rpc;

import java.lang.reflect.Proxy;

/**
 * @author: James Yin
 * @description:
 * @date: 2/10/2020 10:50 PM
 */
public class RpcProxyClient {
    public <T> T clientProxy(Class interfaceCls, String host, int port) {
        return (T) Proxy.newProxyInstance(interfaceCls.getClassLoader(),
                new Class[]{interfaceCls},
                new RemoteInvocationHandler(host, port));
    }
}