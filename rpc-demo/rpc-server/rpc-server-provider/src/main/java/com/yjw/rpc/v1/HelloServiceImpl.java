package com.yjw.rpc.v1;

import com.yjw.rpc.IHelloService;

/**
 * @author: James Yin
 * @description:
 * @date: 2/10/2020 9:45 PM
 */
public class HelloServiceImpl implements IHelloService {

    @Override
    public String sayHello(String content) {
        System.out.println("request content: " + content);
        return "say hello";
    }
}