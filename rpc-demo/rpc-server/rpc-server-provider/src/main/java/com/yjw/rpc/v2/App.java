package com.yjw.rpc.v2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author: James Yin
 * @description:
 * @date: 2/11/2020 11:01 PM
 */
public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("com.yjw.rpc.v2");
        context.start();
    }
}