package com.yjw.rpc.v2;

import com.yjw.rpc.IHelloService;
import org.springframework.stereotype.Component;

/**
 * @author: James Yin
 * @description:
 * @date: 2/10/2020 9:45 PM
 */
@RpcService(IHelloService.class)
public class HelloServiceImpl implements IHelloService {

    @Override
    public String sayHello(String content) {
        System.out.println("request content: " + content);
        return "say hello";
    }
}