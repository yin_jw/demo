package com.yjw.rpc.v1;

import com.yjw.rpc.IHelloService;

/**
 * @author: James Yin
 * @description:
 * @date: 2/10/2020 10:36 PM
 */
public class App {

    public static void main(String[] args) {
        IHelloService helloService = new HelloServiceImpl();
        RpcProxyServer rpcProxyServer = new RpcProxyServer();
        rpcProxyServer.addServices(IHelloService.class.getName(), helloService);
        rpcProxyServer.publish(9090);
    }
}