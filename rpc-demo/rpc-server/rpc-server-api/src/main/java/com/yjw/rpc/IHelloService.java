package com.yjw.rpc;

/**
 * @author: James Yin
 * @description:
 * @date: 2/10/2020 9:40 PM
 */
public interface IHelloService {

    String sayHello(String content);
}