package com.yjw.rpc;

import java.io.Serializable;

/**
 * @author: James Yin
 * @description:
 * @date: 2/10/2020 10:13 PM
 */
public class RpcRequest implements Serializable {

    private static final long serialVersionUID = -5282140409640860434L;
    private String className;
    private String methodName;
    private Object[] parameters;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
