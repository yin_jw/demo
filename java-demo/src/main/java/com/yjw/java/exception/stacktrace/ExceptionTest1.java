package com.yjw.java.exception.stacktrace;

public class ExceptionTest1 {
    public static void main(String args[]) {
        try {
            a();
        } catch (HighLevelException e) {
            //  e.printStackTrace();
            System.out.println(LoggerHelper.printStackTrace(e, 1));
        }
    }

    static void a() throws HighLevelException {
        try {
            b();
        } catch (MidLevelException e) {
            throw new HighLevelException(e);
        }
    }

    static void b() throws MidLevelException {
        c();
    }

    static void c() throws MidLevelException {
        try {
            d();
        } catch (LowLevelException e) {
            throw new MidLevelException(e);
        }
    }

    static void d() throws LowLevelException {
        e();
    }

    static void e() throws LowLevelException {
        throw new LowLevelException();
    }
}

class HighLevelException extends Exception {
    HighLevelException(Throwable cause) {
        super(cause);
    }
}

class MidLevelException extends Exception {
    MidLevelException(Throwable cause) {
        super(cause);
    }
}

class LowLevelException extends Exception {
}