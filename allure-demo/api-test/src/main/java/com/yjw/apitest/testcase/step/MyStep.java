package com.yjw.apitest.testcase.step;

import com.yjw.apitest.common.LoggingInterceptor;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class MyStep {

    @Step("step1")
    public void step1(){
        String url = "https://ynuf.alipay.com/service/um.json";
        OkHttpClient okHttpClient = new OkHttpClient()
                .newBuilder()
                .addInterceptor(new LoggingInterceptor())
                .build();
        final Request request = new Request.Builder()
                .url(url)
                .get() //默认就是GET请求，可以不写
                .build();
        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            System.out.println(response.code());
            System.out.println("run: " + response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step("step2")
    public void step2(){
        String url = "https://ynuf.alipay.com/service/um2.json";
        OkHttpClient okHttpClient = new OkHttpClient()
                .newBuilder()
                .addInterceptor(new LoggingInterceptor())
                .build();
        final Request request = new Request.Builder()
                .url(url)
                .get() //默认就是GET请求，可以不写
                .build();
        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            System.out.println(response.code());
            System.out.println("run: " + response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step("step3")
    public void step3(String name){
        String url = "https://ynuf.alipay.com/service/um.json";
        OkHttpClient okHttpClient = new OkHttpClient()
                .newBuilder()
                .addInterceptor(new LoggingInterceptor())
                .build();
        final Request request = new Request.Builder()
                .url(url)
                .get() //默认就是GET请求，可以不写
                .build();
        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            System.out.println(response.code());
            System.out.println("run: " + response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
