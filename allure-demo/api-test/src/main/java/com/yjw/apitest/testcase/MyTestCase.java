package com.yjw.apitest.testcase;

import com.yjw.apitest.testcase.step.MyStep;
import io.qameta.allure.Allure;
import okhttp3.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import sun.rmi.runtime.Log;

import java.io.IOException;

@Test
public class MyTestCase {

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("方法执行之前");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("方法执行之后");
    }

    @Test
    public void testSomething() {
        MyStep myStep = new MyStep();
        myStep.step1();
        myStep.step2();
        myStep.step3("step3");
    }
}