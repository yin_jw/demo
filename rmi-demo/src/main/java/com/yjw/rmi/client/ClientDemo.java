package com.yjw.rmi.client;

import com.yjw.rmi.server.IHelloService;

import java.rmi.Naming;

public class ClientDemo {
    public static void main(String[] args) {
        try {
            IHelloService helloService =
                    (IHelloService) Naming.lookup("rmi://127.0.0.1:2000/hello");
            System.out.println(helloService.sayHello("zhangsan"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
