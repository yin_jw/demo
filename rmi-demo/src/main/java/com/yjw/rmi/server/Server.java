package com.yjw.rmi.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class Server {
    public static void main(String[] args) {
        // 发布服务
        try {
            IHelloService helloService = new HelloServiceImpl();
            LocateRegistry.createRegistry(2000);
            Naming.rebind("rmi://127.0.0.1:2000/hello", helloService);
            System.out.println("服务启动成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

