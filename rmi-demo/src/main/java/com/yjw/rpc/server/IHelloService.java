package com.yjw.rpc.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IHelloService {

    String sayHello(String msg);
}
