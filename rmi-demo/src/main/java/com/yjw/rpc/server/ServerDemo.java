package com.yjw.rpc.server;

public class ServerDemo {
    public static void main(String[] args) {
        RpcServer rpcServer = new RpcServer();
        IHelloService helloService = new HelloServiceImpl();
        rpcServer.publisher(helloService, 9999);
    }
}
