package com.yjw.rpc.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class HelloServiceImpl implements IHelloService {

    public String sayHello(String msg) {
        return "Hello " + msg;
    }
}
