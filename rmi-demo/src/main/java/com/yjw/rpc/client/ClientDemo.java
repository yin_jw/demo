package com.yjw.rpc.client;

import com.yjw.rpc.server.IHelloService;

public class ClientDemo {
    public static void main(String[] args) {
        RpcClientProxy rpcClientProxy = new RpcClientProxy(IHelloService.class, "localhost", 9999);
        IHelloService helloService = rpcClientProxy.clientProxy();
        System.out.println(helloService.sayHello("zhangsan"));
    }
}
