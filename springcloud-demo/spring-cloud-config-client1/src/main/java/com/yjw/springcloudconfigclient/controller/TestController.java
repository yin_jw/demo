package com.yjw.springcloudconfigclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Value("${from}")
    private String from;

    @GetMapping("from")
    public String from() {
        return from;
    }
}
