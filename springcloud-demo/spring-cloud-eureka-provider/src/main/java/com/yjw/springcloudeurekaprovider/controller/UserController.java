package com.yjw.springcloudeurekaprovider.controller;

import com.yjw.springcloudeurekaprovider.pojo.query.UserQuery;
import com.yjw.springcloudeurekaprovider.pojo.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    @GetMapping("/user/{id}")
    public UserVO getUserById(@PathVariable("id") Long id) {
        System.out.println("GET请求，用户id：" + id);
        return new UserVO(id, "");
    }

    @GetMapping("/user/getUserByName")
    public UserVO getUserByName(@RequestParam("name") String name) {
        System.out.println("GET请求，用户name：" + name);
        return new UserVO(null, name);
    }

    @GetMapping("/user/getUser")
    public UserVO getUser(UserQuery userQuery) {
        System.out.println("GET请求，用户id：" + userQuery.getId() + "，用户name：" + userQuery.getName());
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userQuery, userVO);
        return userVO;
    }

    @PutMapping("/user/update")
    public UserVO updateUser(UserQuery userQuery) {
        System.out.println("PUT请求，用户id：" + userQuery.getId() + "，用户name：" + userQuery.getName());
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userQuery, userVO);
        return userVO;
    }

    @PostMapping("/user/add")
    public UserVO addUser(UserQuery userQuery) {
        System.out.println("POST请求，用户id：" + userQuery.getId() + "，用户name：" + userQuery.getName());
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userQuery, userVO);
        return userVO;
    }

    @DeleteMapping("/user/{id}")
    public void deleteUserById(@PathVariable("id") Long id) {
        System.out.println("DELETE请求，用户id：" + id);
    }
}
