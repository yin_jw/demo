package com.yjw.springcloudeurekaprovider.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/hello")
    public String hello() {
        LOGGER.info("**********************************");
        discoveryClient.getInstances("eureka-provider").forEach(instance -> {
            LOGGER.info("serviceId: " + instance.getServiceId() + ", host: " + instance.getHost() + ", port: " + instance.getPort());
        });
        LOGGER.info("**********************************");
        return "eureka provider";
    }
}
