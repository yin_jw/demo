package com.yjw.springcloudfeignconsumer.manager.client;

import com.yjw.springcloudfeignconsumer.pojo.query.UserQuery;
import com.yjw.springcloudfeignconsumer.pojo.vo.UserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.*;

/**
 * eureka-provider 是服务名，不区分大小写
 */
@FeignClient(name = "eureka-provider", fallback = ProviderFallback.class)
public interface ProviderClient {

    @GetMapping("/user/{id}")
    UserVO getUserById(@PathVariable("id") Long id);

    @GetMapping("/user/getUserByName")
    UserVO getUserByName(@RequestParam("name") String name);

    @GetMapping("/user/getUser")
    UserVO getUser(@SpringQueryMap UserQuery userQuery);

    @PutMapping("/user/update")
    UserVO updateUser(UserQuery userQuery);

    @PostMapping("/user/add")
    UserVO addUser(UserQuery userQuery);

    @DeleteMapping("/user/{id}")
    void deleteUserById(@PathVariable("id") Long id);
}