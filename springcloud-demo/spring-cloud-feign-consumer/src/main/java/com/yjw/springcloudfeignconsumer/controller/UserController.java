package com.yjw.springcloudfeignconsumer.controller;

import com.yjw.springcloudfeignconsumer.pojo.query.UserQuery;
import com.yjw.springcloudfeignconsumer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public void getUserById(@PathVariable("id") Long id) {
        userService.getUserById(id);
    }

    @GetMapping("/user/getUserByName")
    public void getUserByName(@RequestParam("name") String name) {
        userService.getUserByName(name);
    }

    @GetMapping("/user/getUser")
    public void getUser(UserQuery userQuery) {
        userService.getUser(userQuery);
    }

    @PutMapping("/user/update")
    public void updateUser(UserQuery userQuery) {
        userService.updateUser(userQuery);
    }

    @PostMapping("/user/add")
    public void addUser(UserQuery userQuery) {
        userService.addUser(userQuery);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUserById(@PathVariable("id") Long id) {
        userService.deleteUserById(id);
    }
}
