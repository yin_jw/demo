package com.yjw.springcloudfeignconsumer.manager.client;

import com.yjw.springcloudfeignconsumer.pojo.query.UserQuery;
import com.yjw.springcloudfeignconsumer.pojo.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * 服务降级
 */
@Component
public class ProviderFallback implements ProviderClient {

    @Override
    public UserVO getUserById(Long id) {
        System.out.println("getUserById 服务降级");
        return new UserVO(id, "");
    }

    @Override
    public UserVO getUserByName(String name) {
        System.out.println("getUserByName 服务降级");
        return new UserVO(null, name);
    }

    @Override
    public UserVO getUser(UserQuery userQuery) {
        System.out.println("getUser 服务降级");
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userQuery, userVO);
        return userVO;
    }

    @Override
    public UserVO updateUser(UserQuery userQuery) {
        System.out.println("updateUser 服务降级");
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userQuery, userVO);
        return userVO;
    }

    @Override
    public UserVO addUser(UserQuery userQuery) {
        System.out.println("addUser 服务降级");
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userQuery, userVO);
        return userVO;
    }

    @Override
    public void deleteUserById(Long id) {
        System.out.println("deleteUserById 服务降级");
    }
}