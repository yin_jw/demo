package com.yjw.springcloudfeignconsumer.service;

import com.yjw.springcloudfeignconsumer.manager.client.ProviderClient;
import com.yjw.springcloudfeignconsumer.pojo.query.UserQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private ProviderClient providerClient;

    public void getUserById(Long id) {
        providerClient.getUserById(id);
    }

    public void getUser(UserQuery userQuery) {
        providerClient.getUser(userQuery);
    }

    public void getUserByName(String name) {
        providerClient.getUserByName(name);
    }

    public void updateUser(UserQuery userQuery) {
        providerClient.updateUser(userQuery);
    }

    public void addUser(UserQuery userQuery) {
        providerClient.addUser(userQuery);
    }

    public void deleteUserById(Long id) {
        providerClient.deleteUserById(id);
    }

}
