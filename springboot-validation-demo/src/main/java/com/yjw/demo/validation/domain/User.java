package com.yjw.demo.validation.domain;

import com.yjw.demo.validation.validation.constraints.ValidCardNumber;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

public class User {

    @NotNull
    @Max(value = 10000)
    private Long id;

    @NotNull
    private String name;

    /**
     * 卡号：CARD-123456789
     */
    @NotNull
    @ValidCardNumber
    private String cardNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
