package com.yjw.demo.validation.validation;

import com.yjw.demo.validation.validation.constraints.ValidCardNumber;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * {@link ValidCardNumber} {@link ConstraintValidator} 实现
 */
public class ValidCardNumberConstraintValidator implements ConstraintValidator<ValidCardNumber, String> {

    /**
     * 卡号校验
     * <p>
     * 前缀：CARD-
     * 后缀：数字
     *
     * @param value
     * @param context
     * @return
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // 分割字符串
        // Spring：StringUtils.delimitedListToStringArray(value, "-")
        // Java：StringTokenizer
        // Apache commons：StringUtils

        // 不建议使用
        // value.split()
        // 1.易出现 NullPointerException
        // 2.split() 方法使用的是正则表达式，特殊字符需要转义
        String[] parts = StringUtils.split("-");
        if (ArrayUtils.getLength(parts) != 2) {
            return false;
        }
        String prefix = parts[0];
        String suffix = parts[1];

        // Objects.equals(prefix, "CARD-");
        boolean isValidPrefix = prefix.equals("CARD");
        boolean isValidSuffix = StringUtils.isNumeric(suffix);

        return isValidPrefix && isValidSuffix;
    }
}
