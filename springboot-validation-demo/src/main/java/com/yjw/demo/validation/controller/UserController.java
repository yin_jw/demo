package com.yjw.demo.validation.controller;

import com.yjw.demo.validation.domain.User;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UserController {

    /**
     * 新增
     *
     * @param user 用户
     * @return 用户
     */
    @PostMapping("/user/save")
    public User save(@Valid @RequestBody User user) {

        return user;
    }

    /**
     * 更新
     *
     * @param user 用户
     * @return 用户
     */
    @PutMapping("/user/update")
    public User update(@RequestBody User user) {
        // 其他方式
        // API 调用的方式
        Assert.hasText(user.getName(), "用户名称不能为空");
        // JVM 断言
        assert user.getId() <= 10000;

        return user;
    }

}
