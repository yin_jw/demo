package com.yjw.demo.pattern.spi;

import java.util.ServiceLoader;

public class SPITest {

    public static void main(String[] args) {
        ServiceLoader<SqlDriver> loaders = ServiceLoader.load(SqlDriver.class);
        loaders.forEach(loader -> {
            loader.getConnection();
        });
    }
}
