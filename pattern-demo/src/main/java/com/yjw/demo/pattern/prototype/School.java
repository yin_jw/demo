package com.yjw.demo.pattern.prototype;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 学校对象
 * 
 * @author yinjianwei
 * @date 2018/12/06
 */
public class School implements Serializable {

    private static final long serialVersionUID = 1L;

    private String schoolName;

    public School() {
        schoolName = new String("苏州小学");
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

}
