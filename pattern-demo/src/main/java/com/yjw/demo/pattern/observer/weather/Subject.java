package com.yjw.demo.pattern.observer.weather;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 主题
 */
public interface Subject {

    /**
     * 观察者集合
     */
    List<Observer> OBSERVERS = new CopyOnWriteArrayList<>();

    /**
     * 注册观察者
     *
     * @param observer
     */
    default void registerObserver(Observer observer) {
        OBSERVERS.add(observer);
    }

    /**
     * 移除观察者
     *
     * @param observer
     */
    default void removeObserver(Observer observer) {
        int i = OBSERVERS.indexOf(observer);
        if (i >= 0) {
            OBSERVERS.remove(observer);
        }
    }


    /**
     * 通知观察者
     */
    default void notifyObservers() {
        OBSERVERS.forEach(observer -> {
            observer.update(this);
        });
    }

}
