package com.yjw.demo.pattern.proxy.staticed;

import com.yjw.demo.pattern.proxy.RealObject;

public class Test {
    public static void main(String[] args) {
        StaticProxy staticProxy = new StaticProxy(new RealObject());
        staticProxy.doSomething();
    }
}
