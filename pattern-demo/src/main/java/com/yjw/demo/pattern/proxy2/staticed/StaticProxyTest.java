package com.yjw.demo.pattern.proxy2.staticed;

public class StaticProxyTest {
    public static void main(String[] args) {
        Father father = new Father(new Son());
        father.findLove();
    }
}
