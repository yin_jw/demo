package com.yjw.demo.pattern.factory.function;

import com.yjw.demo.pattern.factory.Milk;
import com.yjw.demo.pattern.factory.Yili;

public class YiliFactory implements Factory {

    @Override
    public Milk getMilk() {
        return new Yili();
    }

}
