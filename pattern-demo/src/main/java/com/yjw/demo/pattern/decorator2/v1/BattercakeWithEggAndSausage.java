package com.yjw.demo.pattern.decorator2.v1;

public class BattercakeWithEggAndSausage extends BattercakeWithEgg {

    @Override
    protected String getMsg() {
        return super.getMsg() + "+1跟香肠";
    }

    /**
     * 加1根香肠加2元钱
     *
     * @return
     */
    @Override
    public int getPrice() {
        return super.getPrice() + 2;
    }
}