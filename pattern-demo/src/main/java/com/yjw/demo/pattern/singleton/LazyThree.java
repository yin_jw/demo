package com.yjw.demo.pattern.singleton;

/**
 * 懒汉式，双重检查锁
 * 
 * <pre>
 * https://www.cnblogs.com/xz816111/p/8470048.html
 * </pre>
 * 
 * @author yinjianwei
 * @date 2018/12/09
 */
public class LazyThree {

    private LazyThree() {}

    private volatile static LazyThree lazyThree = null;

    public static LazyThree getInstance() {
        if (lazyThree == null) {
            synchronized (LazyThree.class) {
                if (lazyThree == null) {
                    lazyThree = new LazyThree();
                }
            }
        }
        return lazyThree;
    }
}
