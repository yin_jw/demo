package com.yjw.demo.pattern.singleton;

/**
 * 懒汉式，静态内部类
 * 
 * <pre>
 * 静态内部类不会随着外部类一起加载，它只有当被调用的时候才开始加载，所以不占内存，
 * 这种方式不但能确保线程安全，也能延迟了单例的实例化
 * </pre>
 * 
 * @author yinjianwei
 * @date 2018/12/04
 */
public class LazyFour {

    private LazyFour() {};

    public static LazyFour getInstance() {
        return LazyHolder.lazyThree;
    }

    private static class LazyHolder {
        private static final LazyFour lazyThree = new LazyFour();
    }

}