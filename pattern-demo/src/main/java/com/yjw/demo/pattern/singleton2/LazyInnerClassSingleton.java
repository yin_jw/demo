package com.yjw.demo.pattern.singleton2;

/**
 * 这种形式兼顾饿汉式单例模式的内存浪费问题和synchronized的性能问题
 * 完美地屏蔽了这两个缺点
 */
public class LazyInnerClassSingleton {

    private LazyInnerClassSingleton() {
        if (LazyHolder.INSTANCE != null) {
            throw new RuntimeException("不允许创建多个实例");
        }
    }

    // 注意关键字final，保证方法不被重写和重载
    public static final LazyInnerClassSingleton getInstance() {
        // 在返回结果之前，会先加载内部类
        return LazyHolder.INSTANCE;
    }

    // 默认不加载
    private static class LazyHolder {
        private static final LazyInnerClassSingleton INSTANCE = new LazyInnerClassSingleton();
    }
}