package com.yjw.demo.pattern.singleton2;

import java.io.Serializable;

public class SeriableSingleton implements Serializable {
    private SeriableSingleton(){}
    private static final SeriableSingleton instance = new SeriableSingleton();
    public static SeriableSingleton getInstance(){
        return instance;
    }
    private Object readResolve(){
        return instance;
    }
}