package com.yjw.demo.pattern.proxy2.staticed;

public class Son implements Person {
    @Override
    public void findLove() {
        System.out.println("儿子要求：肤白貌美大长腿");
    }
}
