package com.yjw.demo.pattern.strategy;

import com.yjw.demo.pattern.strategy.pay.Payment;

/**
 * 模拟订单支付场景
 * 
 * @author yinjianwei
 * @date 2018/12/13
 */
public class Order {

    private String uid;
    private String orderId;
    private Double amount;

    public Order(String uid, String orderId, Double amount) {
        this.uid = uid;
        this.orderId = orderId;
        this.amount = amount;
    }

    public PayState pay() {
        return pay(PayStrategy.DEFAULT_PAY);
    }

    public PayState pay(String payKey) {
        Payment payment = PayStrategy.get(payKey);
        System.out.println("欢迎使用" + payment.getName());
        System.out.println("本次交易金额为：" + amount + "，开始扣款。。。");
        return payment.pay(uid, amount);
    }
}