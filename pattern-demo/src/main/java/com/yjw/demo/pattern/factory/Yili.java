package com.yjw.demo.pattern.factory;

/**
 * 伊利牛奶
 * 
 * @author yinjianwei
 * @date 2018/11/30
 */
public class Yili implements Milk {

    @Override
    public String getName() {
        return "伊利";
    }

}
