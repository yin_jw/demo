package com.yjw.demo.pattern.proxy2.template;

public class Client {
    public static void main(String[] args) {
        Subject subject = new Proxy(new RealSubject());
        subject.request();
    }
}
