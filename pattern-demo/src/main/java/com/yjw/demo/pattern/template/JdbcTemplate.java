package com.yjw.demo.pattern.template;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

/**
 * 模板方法模式：固定的执行步骤，可以参与自定义其中一部分内容
 * 
 * 比如这里可以自定义解析结果集
 * 
 * @author yinjianwei
 * @date 2018/12/21
 */
public class JdbcTemplate {

    private DataSource dataSource;

    public JdbcTemplate(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> List<T> executeQuery(String sql, RowMapper<T> rowMapper) {
        List<T> result = null;
        try {
            // 1.获取链接
            Connection conn = dataSource.getConnection();
            // 2.创建语句集
            PreparedStatement ps = conn.prepareStatement(sql);
            // 3.执行语句，并且获得结果集
            ResultSet rs = ps.executeQuery();
            // 4.解析结果集
            result = parseResultSet(rs, rowMapper);
            // 5.关闭结果集
            rs.close();
            // 6.关闭语句集
            ps.close();
            // 7.关闭链接

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 解析语句集
     * 
     * @param rs
     * @return
     * @throws SQLException
     */
    private <T> List<T> parseResultSet(ResultSet rs, RowMapper<T> rowMapper) throws SQLException {
        List<T> result = new ArrayList<>();
        while (rs.next()) {
            result.add(rowMapper.mapRow(rs));
        }
        return result;
    }

}
