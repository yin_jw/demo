package com.yjw.demo.pattern.proxy2.jdk;

import com.yjw.demo.pattern.proxy2.staticed.Person;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JDKMeipo implements InvocationHandler {

    private Person person;

    public JDKMeipo(Person person) {
        this.person = person;
    }

    public Person getInstance() {
        return (Person) Proxy.newProxyInstance(Person.class.getClassLoader(),
                person.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object object = method.invoke(person, args);
        after();
        return object;
    }

    private void before() {
        System.out.println("我是媒婆：我要给你找对象，现在已经确认你的需求");
        System.out.println("开始物色");
    }

    private void after() {
        System.out.println("如果合适的话，就准备办事");
    }
}
