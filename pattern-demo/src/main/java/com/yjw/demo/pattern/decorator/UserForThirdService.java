package com.yjw.demo.pattern.decorator;

/**
 * 适配器模式，扩展兼容第三方登录场景
 * 
 * @author yinjianwei
 * @date 2018/12/23
 */
public class UserForThirdService implements IUserForThirdService {

    private IUserService userService;

    public UserForThirdService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public ResultMessage register(String userName, String password) {
        return userService.register(userName, password);
    }

    @Override
    public ResultMessage login(String userName, String password) {
        return userService.login(userName, password);
    }

    public ResultMessage loginByQQ(String openId) {
        // 1.openId作为用户名，密码为null，注册用户
        this.register(openId, null);
        // 2.登录
        return this.login(openId, null);
    }

    public ResultMessage loginByWechat(String openId) {
        return null;
    }

    public ResultMessage loginByWeibo(String openId) {
        return null;
    }

}
