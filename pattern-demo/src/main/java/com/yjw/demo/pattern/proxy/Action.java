package com.yjw.demo.pattern.proxy;

/**
 * 接口
 * 
 * @author yinjianwei
 * @date 2018/12/10
 */
public interface Action {
    public void doSomething();
}