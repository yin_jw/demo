package com.yjw.demo.pattern.decorator2.v2;

public class BattercakeTest {
    public static void main(String[] args) {
        Battercake battercake;
        // 路边摊买一个煎饼
        battercake = new BaseBattercake();
        // 加1个鸡蛋
        battercake = new EggDecorator(battercake);
        // 再加1个鸡蛋
        battercake = new EggDecorator(battercake);
        // 再加1根香肠
        battercake = new SausageDecorator(battercake);

        System.out.println(battercake.getMsg() + "，总价格：" + battercake.getPrice());
    }
}
