package com.yjw.demo.pattern.template2;

public class BigDataCourse extends NetworkCourse {

    private boolean needHomeworkFlag = false;

    public BigDataCourse(boolean needHomeworkFlag) {
        this.needHomeworkFlag = needHomeworkFlag;
    }

    @Override
    protected boolean needHomework() {
        return this.needHomeworkFlag;
    }

    @Override
    protected void checkHomework() {
        System.out.println("检查大数据的课堂作业");
    }
}