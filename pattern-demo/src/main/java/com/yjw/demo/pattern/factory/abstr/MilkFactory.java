package com.yjw.demo.pattern.factory.abstr;

import com.yjw.demo.pattern.factory.Mengniu;
import com.yjw.demo.pattern.factory.Milk;
import com.yjw.demo.pattern.factory.Sanlu;
import com.yjw.demo.pattern.factory.Yili;

/**
 * 牛奶工厂类
 * 
 * @author yinjianwei
 * @date 2018/12/01
 */
public class MilkFactory extends AbstractFactory {

    @Override
    public Milk getMengniu() {
        return new Mengniu();
    }

    @Override
    public Milk getSanlu() {
        return new Sanlu();
    }

    @Override
    public Milk getYili() {
        return new Yili();
    }

    // 也可以结合工厂方法模式一起使用

    /*@Override
    public Milk getMengniu() {
        return new MengniuFactory().getMilk();
    }
    
    @Override
    public Milk getSanlu() {
        return new SanluFactory().getMilk();
    }
    
    @Override
    public Milk getYili() {
        return new YiliFactory().getMilk();
    }*/

}
