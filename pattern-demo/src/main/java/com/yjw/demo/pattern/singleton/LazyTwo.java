package com.yjw.demo.pattern.singleton;

/**
 * 懒汉式，加线程安全锁
 * 
 * <pre>
 * 1.能达到延迟加载实例的效果
 * 2.相比LazyOne，在方法上加了同步锁synchronized，保证了在多线程环境下线程安全
 * 3.在方法上加同步锁synchronized，会导致很大的性能开销，并且加锁其实只需要在第一次初始化的时候用到，之后的调用都没必要再进行加锁
 * </pre>
 * 
 * @author yinjianwei
 * @date 2018/12/04
 */
public class LazyTwo {

    private LazyTwo() {}

    private static LazyTwo lazyTwo = null;

    /**
     * 获取实例
     * 
     * @return
     */
    public static synchronized LazyTwo getInstance() {
        if (lazyTwo == null) {
            // 测试线程不安全的时候使用
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lazyTwo = new LazyTwo();
        }
        return lazyTwo;
    }
}
