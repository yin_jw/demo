package com.yjw.demo.pattern.prototype;

import java.io.*;

/**
 * 学生对象
 * 
 * @author yinjianwei
 * @date 2018/12/06
 */
public class Student implements Cloneable, Serializable {

    private static final long serialVersionUID = 1L;
    private String studentName;
    private School school;

    public Student() {
        studentName = new String("张三");
        school = new School();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * 利用序列化、反序列化实现深拷贝
     *
     * @return
     * @throws Exception
     */
    public Student deepClone() throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);

        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);

        return (Student)ois.readObject();
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

}
