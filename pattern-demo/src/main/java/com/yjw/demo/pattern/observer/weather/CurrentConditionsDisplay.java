package com.yjw.demo.pattern.observer.weather;

import javafx.beans.value.WeakChangeListener;

/**
 * 目前状况布告板
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {

    private float temperature;
    private float humidity;
    private Subject weatherData;

    /**
     * 注册观察者
     *
     * @param subject
     */
    public CurrentConditionsDisplay(Subject subject) {
        this.weatherData = subject;
        subject.registerObserver(this);
    }

    @Override
    public void update(Subject subject) {
        if (!(subject instanceof WeatherData)) {
            return;
        }
        WeatherData weatherData = (WeatherData) subject;
        this.temperature = weatherData.getTemperature();
        this.humidity = weatherData.getHumidity();
        display();
    }

    @Override
    public void display() {
        System.out.println("**************************************************");
        System.out.println("Current conditions: " + temperature
                + "F degrees and " + humidity + "% humidity");
        System.out.println("**************************************************");
    }

}
