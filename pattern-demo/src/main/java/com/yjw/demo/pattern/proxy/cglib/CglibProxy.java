package com.yjw.demo.pattern.proxy.cglib;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * CGLIB 动态代理类，不需要接口
 * 
 * @author yinjianwei
 * @date 2018/12/10
 */
public class CglibProxy implements MethodInterceptor {

    public Object getInstance(Class<?> clzss) {
        // 创建加强器，用来创建动态代理类
        Enhancer enhancer = new Enhancer();
        // 为加强器指定要代理的业务类
        enhancer.setSuperclass(clzss);
        // 设置回调：对于代理类上所有方法的调用，都会调用CallBack，而Callback则需要实现intercept()方法进行拦截
        enhancer.setCallback(this);
        // 创建动态代理类对象并返回
        return enhancer.create();
    }

    /**
     * 实现回调方法
     */
    @Override
    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("执行前");
        methodProxy.invokeSuper(proxy, args);
        System.out.println("执行前");
        return null;
    }

}
