package com.yjw.demo.pattern.proxy.jdk;

import com.yjw.demo.pattern.proxy.Action;
import com.yjw.demo.pattern.proxy.RealObject;

public class Test {
    public static void main(String[] args) {
        JdkProxy jdkProxy = new JdkProxy();
        Action proxy = (Action)jdkProxy.getInstance(new RealObject());
        proxy.doSomething();
    }
}
