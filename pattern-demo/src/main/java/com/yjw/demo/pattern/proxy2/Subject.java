package com.yjw.demo.pattern.proxy2;

public interface Subject {
        public void request();
}