package com.yjw.demo.pattern.singleton;

/**
 * 懒汉式
 * 
 * <pre>
 * 1.能达到延迟加载实例的效果
 * 2.在多线程环境下，可能会产生多个实例，线程不安全
 * </pre>
 * 
 * @author yinjianwei
 * @date 2018/12/04
 */
public class LazyOne {

    private LazyOne() {}

    private static LazyOne lazyOne = null;

    /**
     * 获取实例
     * 
     * @return
     */
    public static LazyOne getInstance() {
        if (lazyOne == null) {
            // 测试线程不安全的时候使用
            /*try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            lazyOne = new LazyOne();
        }
        return lazyOne;
    }
}
