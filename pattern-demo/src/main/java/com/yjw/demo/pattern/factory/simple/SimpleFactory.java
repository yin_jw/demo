package com.yjw.demo.pattern.factory.simple;

import com.yjw.demo.pattern.factory.Mengniu;
import com.yjw.demo.pattern.factory.Milk;
import com.yjw.demo.pattern.factory.Sanlu;
import com.yjw.demo.pattern.factory.Yili;

/**
 * 简单工厂模式
 *
 * @author yinjianwei
 * @date 2018/11/30
 */
public class SimpleFactory {

    /**
     * 生产牛奶
     *
     * @return
     */
    public Milk getMilk(String name) {
        Milk milk = null;
        if (name.equals("蒙牛")) {
            milk = new Mengniu();
        }
        if (name.equals("三鹿")) {
            milk = new Sanlu();
        }
        if (name.equals("伊利")) {
            milk = new Yili();
        }
        return milk;
    }
}
