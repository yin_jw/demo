package com.yjw.demo.pattern.prototype;

/**
 * 测试类
 * 
 * @author yinjianwei
 * @date 2018/12/06
 */
public class PrototypeTest {

    public static void main(String[] args) throws Exception {
        // shallowClone();
        deepClone();
    }

    /**
     * 浅克隆
     */
    private static void shallowClone() throws Exception {
        System.out.println("*************浅克隆*************");

        Student s1 = new Student();
        Student s2 = (Student) s1.clone();
        System.out.println(s1 == s2);
        System.out.println(s1.getSchool() == s2.getSchool());
        System.out.println(s1.getSchool());
        System.out.println(s2.getSchool());
    }

    /**
     * 深克隆
     */
    private static void deepClone() throws Exception {
        System.out.println("*************深克隆*************");

        Student s1 = new Student();
        Student s2 = s1.deepClone();
        System.out.println(s1 == s2);
        System.out.println(s1.getSchool() == s2.getSchool());
        System.out.println(s1.getSchool());
        System.out.println(s2.getSchool());
    }

}
