package com.yjw.demo.pattern.decorator;

/**
 * 装饰器模式
 * 
 * @author yinjianwei
 * @date 2018/12/23
 */
public interface IUserForThirdService extends IUserService {

    public ResultMessage loginByQQ(String openId);

    public ResultMessage loginByWechat(String openId);

    public ResultMessage loginByWeibo(String openId);
}
