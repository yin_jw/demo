package com.yjw.demo.pattern.factory2.method;

import com.yjw.demo.pattern.factory2.simple.ICourse;

public interface ICourseFactory {
    ICourse crete();
}