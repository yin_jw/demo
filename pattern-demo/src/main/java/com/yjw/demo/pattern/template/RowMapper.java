package com.yjw.demo.pattern.template;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<T> {
    /**
     * 解析结果
     * 
     * @param rs
     * @throws SQLException
     */
    T mapRow(ResultSet rs) throws SQLException;
}
