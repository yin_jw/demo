package com.yjw.demo.pattern.strategy;

import com.yjw.demo.pattern.strategy.pay.AliPay;
import com.yjw.demo.pattern.strategy.pay.Payment;
import com.yjw.demo.pattern.strategy.pay.UnionPay;
import com.yjw.demo.pattern.strategy.pay.WechatPay;

import java.util.HashMap;
import java.util.Map;

public class PayStrategy {
    public static final String DEFAULT_PAY = "AliPay";
    public static final String ALI_PAY = "AliPay";
    public static final String WECHAT_PAY = "WechatPay";
    public static final String UNION_PAY = "UnionPay";

    private static Map<String, Payment> payStrategy = new HashMap<>();

    static {
        payStrategy.put(DEFAULT_PAY, new AliPay());
        payStrategy.put(ALI_PAY, new AliPay());
        payStrategy.put(WECHAT_PAY, new WechatPay());
        payStrategy.put(UNION_PAY, new UnionPay());
    }

    public static Payment get(String payKey) {
        if (!payStrategy.containsKey(payKey)) {
            return payStrategy.get(DEFAULT_PAY);
        }
        return payStrategy.get(payKey);
    }
}