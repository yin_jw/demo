package com.yjw.demo.pattern.decorator2.v2;

public abstract class Battercake {
    protected abstract String getMsg();
    protected abstract int getPrice();
}