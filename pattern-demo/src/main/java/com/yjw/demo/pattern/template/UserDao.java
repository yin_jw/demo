package com.yjw.demo.pattern.template;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserDao {
    private JdbcTemplate jdbcTemplate = new JdbcTemplate(null);

    public List<User> query() {
        String sql = "select * from t_user";
        List<User> users = jdbcTemplate.executeQuery(sql, new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet rs) throws SQLException {
                User user = new User();
                user.setId(rs.getLong(1));
                user.setName(rs.getString(2));
                user.setAge(rs.getInt(3));
                return user;
            }
        });
        return users;
    }
}
