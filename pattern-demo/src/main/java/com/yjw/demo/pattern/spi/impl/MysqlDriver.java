package com.yjw.demo.pattern.spi.impl;

import com.yjw.demo.pattern.spi.SqlDriver;

/**
 * MySQL 驱动
 */
public class MysqlDriver implements SqlDriver {

    @Override
    public void getConnection() {
        System.out.println("获取 MySQL 驱动");
    }
}
