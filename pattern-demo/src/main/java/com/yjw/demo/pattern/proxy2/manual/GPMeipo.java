package com.yjw.demo.pattern.proxy2.manual;

import com.yjw.demo.pattern.proxy2.staticed.Person;

import java.lang.reflect.Method;

public class GPMeipo implements GPInvocationHandler {
    // 被代理的对象，把引用保存下来
    private Person person;

    public GPMeipo(Person person) {
        this.person = person;
    }

    public Person getInstance() {
        Class<?> clazz = person.getClass();
        return (Person) GPProxy.newProxyInstance(new GPClassLoader(), clazz.getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        method.invoke(person, args);
        after();
        return null;
    }

    private void before() {
        System.out.println("我是媒婆：我要给你找对象，现在已经确认你的需求");
        System.out.println("开始物色");
    }

    private void after() {
        System.out.println("如果合适的话，就准备办事");
    }
}
