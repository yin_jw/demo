package com.yjw.demo.pattern.proxy2.jdk;

import com.yjw.demo.pattern.proxy2.staticed.Person;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;

/* renamed from: $Proxy0  reason: invalid class name and default package */
public final class C$Proxy0 extends Proxy implements Person {
    private static Method m0;
    private static Method m1;
    private static Method m2;
    private static Method m3;

    static {
        try {
            m1 = Class.forName("java.lang.Object").getMethod("equals", new Class[]{Class.forName("java.lang.Object")});
            m3 = Class.forName("com.yjw.demo.pattern.proxy2.staticed.Person").getMethod("findLove", new Class[0]);
            m2 = Class.forName("java.lang.Object").getMethod("toString", new Class[0]);
            m0 = Class.forName("java.lang.Object").getMethod("hashCode", new Class[0]);
        } catch (NoSuchMethodException e) {
            throw new NoSuchMethodError(e.getMessage());
        } catch (ClassNotFoundException e2) {
            throw new NoClassDefFoundError(e2.getMessage());
        }
    }

    /**
     * 代理类的构造方法，方法参数为InvocationHandler类型，调用父类的构造方法，父类构造方法代码如下所示
     * <p>
     * protected Proxy(InvocationHandler h) {
     *     Objects.requireNonNull(h);
     *     this.h = h;
     * }
     * </p>
     *
     * @param invocationHandler
     */
    public C$Proxy0(InvocationHandler invocationHandler) {
        super(invocationHandler);
    }

    public final boolean equals(Object obj) {
        try {
            return ((Boolean) this.h.invoke(this, m1, new Object[]{obj})).booleanValue();
        } catch (Error | RuntimeException e) {
            throw e;
        } catch (Throwable th) {
            throw new UndeclaredThrowableException(th);
        }
    }

    /**
     * 调用InvocationHandler中的invoke方法，并把m3传进去
     */
    public final void findLove() {
        try {
            this.h.invoke(this, m3, (Object[]) null);
        } catch (Error | RuntimeException e) {
            throw e;
        } catch (Throwable th) {
            throw new UndeclaredThrowableException(th);
        }
    }

    public final int hashCode() {
        try {
            return ((Integer) this.h.invoke(this, m0, (Object[]) null)).intValue();
        } catch (Error | RuntimeException e) {
            throw e;
        } catch (Throwable th) {
            throw new UndeclaredThrowableException(th);
        }
    }

    public final String toString() {
        try {
            return (String) this.h.invoke(this, m2, (Object[]) null);
        } catch (Error | RuntimeException e) {
            throw e;
        } catch (Throwable th) {
            throw new UndeclaredThrowableException(th);
        }
    }
}