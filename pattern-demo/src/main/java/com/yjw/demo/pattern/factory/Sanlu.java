package com.yjw.demo.pattern.factory;

/**
 * 三鹿牛奶
 * 
 * @author yinjianwei
 * @date 2018/11/30
 */
public class Sanlu implements Milk {

    @Override
    public String getName() {
        return "三鹿";
    }

}
