package com.yjw.demo.pattern.factory;

/**
 * 牛奶接口
 * 
 * @author yinjianwei
 * @date 2018/11/30
 */
public interface Milk {

    /**
     * 获取名称
     * 
     * @return
     */
    String getName();
}
