package com.yjw.demo.pattern.proxy2.jdk;

import com.yjw.demo.pattern.proxy2.staticed.Person;

public class Customer implements Person {

    @Override
    public void findLove() {
        System.out.println("高富帅，身高180，有6块腹肌");
    }
}
