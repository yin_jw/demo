package com.yjw.demo.pattern.proxy.staticed;

import com.yjw.demo.pattern.proxy.Action;

/**
 * 静态代理类
 * 
 * @author yinjianwei
 * @date 2018/12/10
 */
public class StaticProxy implements Action {

    private Action action;

    public StaticProxy(Action action) {
        this.action = action;
    }

    @Override
    public void doSomething() {
        System.out.println("执行前");
        action.doSomething();
        System.out.println("执行前");
    }

}
