package com.yjw.demo.pattern.observer.mouse;

import java.lang.reflect.Method;

import com.yjw.demo.pattern.observer.mouse.core.Event;

/**
 * 鼠标事件测试
 *
 * @author yinjianwei
 * @date 2019/01/04
 */
public class MouseEventTest {
    public static void main(String[] args) {
        try {
            MouseEventCallback mouseEventCallback = new MouseEventCallback();

            // 注册事件
            Mouse mouse = new Mouse();
            mouse.addListener(MouseEventType.ON_CLICK, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_DOUBLE_CLICK, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_UP, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_DOWN, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_MOVE, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_WHEEL, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_OVER, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_BLUR, mouseEventCallback);
            mouse.addListener(MouseEventType.ON_FOCUS, mouseEventCallback);

            // 调用方法
            mouse.click();
            mouse.doubleClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
