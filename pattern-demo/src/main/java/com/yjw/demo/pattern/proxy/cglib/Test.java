package com.yjw.demo.pattern.proxy.cglib;

public class Test {
    public static void main(String[] args) {
        CglibProxy cglibProxy = new CglibProxy();
        RealObject1 realObject1 = (RealObject1)cglibProxy.getInstance(RealObject1.class);
        realObject1.doSomething();
        System.out.println(realObject1.getClass());
    }
}
