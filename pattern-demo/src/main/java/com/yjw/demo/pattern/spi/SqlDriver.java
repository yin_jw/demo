package com.yjw.demo.pattern.spi;

import java.sql.Connection;

/**
 * SQL 驱动
 */
public interface SqlDriver {

    public void getConnection();
}
