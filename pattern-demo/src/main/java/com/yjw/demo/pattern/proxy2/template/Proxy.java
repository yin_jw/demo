package com.yjw.demo.pattern.proxy2.template;

public class Proxy implements Subject {
    private Subject subject;

    public Proxy(Subject subject) {
        this.subject = subject;
    }

    public void request() {
        subject.request();
    }

    public void before() {

    }

    public void after() {

    }
}
