package com.yjw.demo.pattern.proxy2.staticed;

public class Father implements Person {

    private Person person;

    public Father(Person person){
        this.person = person;
    }

    @Override
    public void findLove() {
        System.out.println("父亲物色对象");
        person.findLove();
        System.out.println("双方同意交往，确立关系");
    }
}
