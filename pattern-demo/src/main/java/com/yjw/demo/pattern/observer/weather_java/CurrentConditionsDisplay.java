package com.yjw.demo.pattern.observer.weather_java;

import com.yjw.demo.pattern.observer.weather.DisplayElement;

import java.util.Observable;
import java.util.Observer;

/**
 * 目前状况布告板
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {

    private float temperature;
    private float humidity;
    private Observable observable;

    /**
     * 注册观察者
     *
     * @param observable
     */
    public CurrentConditionsDisplay(Observable observable) {
        this.observable = observable;
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (!(o instanceof WeatherData)) {
            return;
        }
        WeatherData weatherData = (WeatherData) o;
        this.temperature = weatherData.getTemperature();
        this.humidity = weatherData.getHumidity();
        display();
    }

    @Override
    public void display() {
        System.out.println("**************************************************");
        System.out.println("Current conditions: " + temperature
                + "F degrees and " + humidity + "% humidity");
        System.out.println("**************************************************");
    }

}
