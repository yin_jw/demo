package com.yjw.demo.pattern.factory2.method;

import com.yjw.demo.pattern.factory2.simple.ICourse;
import com.yjw.demo.pattern.factory2.simple.PythonCourse;

public class PythonCourseFactory implements ICourseFactory {
    @Override
    public ICourse crete() {
        return new PythonCourse();
    }
}
