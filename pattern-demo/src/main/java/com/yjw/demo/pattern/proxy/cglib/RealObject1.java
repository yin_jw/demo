package com.yjw.demo.pattern.proxy.cglib;

/**
 * 被代理类
 * 
 * @author yinjianwei
 * @date 2018/12/10
 */
public class RealObject1 {

    public void doSomething() {
        System.out.println("do something");
    }

}
