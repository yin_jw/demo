package com.yjw.demo.pattern.observer.weather;

/**
 * 观察者
 */
public interface Observer {

    /**
     * 收到通知并自动更新
     *
     */
    public void update(Subject subject);

}
