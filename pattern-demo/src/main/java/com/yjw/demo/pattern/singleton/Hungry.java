package com.yjw.demo.pattern.singleton;

/**
 * 饿汉式
 * 
 * <pre>
 * 1.这种写法比较简单，在类装载的时候就完成实例化。避免了线程同步问题。
 * 2.在类装载的时候就完成实例化，没有达到延迟加载的效果。如果从始至终从未使用过这个实例，则会造成内存的浪费。
 * </pre>
 * 
 * @author yinjianwei
 * @date 2018/12/04
 */
public class Hungry {

    private Hungry() {}

    private static final Hungry hungry = new Hungry();

    /**
     * 获取实例
     * 
     * @return
     */
    public static Hungry getInstance() {
        return hungry;
    }
}
