package com.yjw.demo.pattern.proxy;

/**
 * 被代理类
 * 
 * @author yinjianwei
 * @date 2018/12/10
 */
public class RealObject implements Action {

    @Override
    public void doSomething() {
        System.out.println("do something");
    }

}
