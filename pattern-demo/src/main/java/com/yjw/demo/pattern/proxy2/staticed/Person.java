package com.yjw.demo.pattern.proxy2.staticed;

/**
 * 顶层接口
 */
public interface Person {
    public void findLove();
}
