package com.yjw.demo.pattern.strategy.pay;

import com.yjw.demo.pattern.strategy.PayState;

/**
 * 支付渠道
 */
public abstract class Payment {

    /**
     * 支付类型
     */
    public abstract String getName();

    /**
     * 查询余额
     *
     * @param uid
     * @return
     */
    protected abstract double queryBalance(String uid);

    /**
     * 扣款支付
     *
     * @param uid
     * @param amount
     */
    public PayState pay(String uid, Double amount) {
        if (queryBalance(uid) < amount) {
            return new PayState(500, "支付失败", "余额不足");
        }
        return new PayState(200, "支付成功", "支付金额：" + amount);
    }
}
