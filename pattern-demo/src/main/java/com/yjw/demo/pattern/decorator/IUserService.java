package com.yjw.demo.pattern.decorator;


/**
 * 用户服务
 * 
 * @author yinjianwei
 * @date 2018/12/23
 */
public interface IUserService {
    /**
     * 用户注册
     * 
     * @param userName
     * @param password
     * @return
     */
    public ResultMessage register(String userName, String password);

    /**
     * 用户登录
     * 
     * @param userName
     * @param password
     * @return
     */
    public ResultMessage login(String userName, String password);
}
