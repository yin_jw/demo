package com.yjw.demo.pattern.factory2.abastract;

public class JavaNote implements INote {
    @Override
    public void edit() {
        System.out.println("编写 Java 笔记");
    }
}