package com.yjw.demo.pattern.factory2.method;

import com.yjw.demo.pattern.factory2.simple.ICourse;
import com.yjw.demo.pattern.factory2.simple.JavaCourse;

public class JavaCourseFactory implements ICourseFactory {
    @Override
    public ICourse crete() {
        return new JavaCourse();
    }
}