package com.yjw.demo.pattern.spi.impl;

import com.yjw.demo.pattern.spi.SqlDriver;

/**
 * Oracle 驱动
 */
public class OracleDriver implements SqlDriver {

    @Override
    public void getConnection() {
        System.out.println("获取 Oracle 驱动");
    }
}
