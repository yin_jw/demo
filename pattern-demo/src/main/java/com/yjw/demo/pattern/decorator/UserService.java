package com.yjw.demo.pattern.decorator;

/**
 * 用户服务
 * 
 * @author yinjianwei
 * @date 2018/12/23
 */
public class UserService implements IUserService {

    /**
     * 用户注册
     * 
     * @param userName
     * @param password
     * @return
     */
    public ResultMessage register(String userName, String password) {
        return null;
    }

    /**
     * 用户登录
     * 
     * @param userName
     * @param password
     * @return
     */
    public ResultMessage login(String userName, String password) {
        return null;
    }
}
