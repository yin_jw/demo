package com.yjw.demo.pattern.delegate2;

public interface IEmployee {

    public void doing(String command);
}