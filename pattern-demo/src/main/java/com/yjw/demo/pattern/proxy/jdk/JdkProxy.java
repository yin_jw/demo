package com.yjw.demo.pattern.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.yjw.demo.pattern.proxy.RealObject;

/**
 * JDK 动态代理类
 * 
 * @author yinjianwei
 * @date 2018/12/10
 */
public class JdkProxy implements InvocationHandler {

    private RealObject realObject;

    public Object getInstance(RealObject realObject) {
        this.realObject = realObject;
        Class<?> clzss = realObject.getClass();
        return Proxy.newProxyInstance(clzss.getClassLoader(), clzss.getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("执行前");
        method.invoke(realObject, args);
        System.out.println("执行前");
        return null;
    }

}
