package com.yjw.demo.pattern.delegate2;

public class Boss {

    public void command(String command, Leader leader) {
        leader.doing(command);
    }
}