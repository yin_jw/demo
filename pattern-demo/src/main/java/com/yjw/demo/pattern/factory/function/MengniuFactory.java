package com.yjw.demo.pattern.factory.function;

import com.yjw.demo.pattern.factory.Mengniu;
import com.yjw.demo.pattern.factory.Milk;

public class MengniuFactory implements Factory {

    @Override
    public Milk getMilk() {
        return new Mengniu();
    }

}
