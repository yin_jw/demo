package com.yjw.demo.pattern.factory;

/**
 * 蒙牛牛奶
 * 
 * @author yinjianwei
 * @date 2018/11/30
 */
public class Mengniu implements Milk {

    @Override
    public String getName() {
        return "蒙牛";
    }

}
