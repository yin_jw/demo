package com.yjw.demo.pattern.singleton;

import java.util.concurrent.CountDownLatch;

/**
 * 测试类
 * 
 * @author yinjianwei
 * @date 2018/12/04
 */
@SuppressWarnings("unused")
public class Test {

    public static void main(String[] args) {
        // testLazyOne();
        testLazyTwo();
    }

    /**
     * 测试懒汉式
     */
    private static void testLazyOne() {
        CountDownLatch startSignal = new CountDownLatch(1);
        for (int i = 0; i < 500; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        startSignal.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(LazyOne.getInstance());
                }
            }).start();
        }
        startSignal.countDown();
    }

    /**
     * 测试带锁的懒汉式
     */
    private static void testLazyTwo() {
        CountDownLatch startSignal = new CountDownLatch(1);
        for (int i = 0; i < 500; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        startSignal.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(LazyTwo.getInstance());
                }
            }).start();
        }
        startSignal.countDown();
    }
}
