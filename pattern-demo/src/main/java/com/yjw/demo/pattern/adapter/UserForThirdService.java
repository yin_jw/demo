package com.yjw.demo.pattern.adapter;

/**
 * 适配器模式，扩展兼容第三方登录场景
 * 
 * @author yinjianwei
 * @date 2018/12/23
 */
public class UserForThirdService extends UserService {

    public ResultMessage loginByQQ(String openId) {
        // 1.openId作为用户名，密码为null，注册用户
        super.register(openId, null);
        // 2.登录
        return super.login(openId, null);
    }

    public ResultMessage loginByWechat(String openId) {
        return null;
    }

    public ResultMessage loginByWeibo(String openId) {
        return null;
    }
}
