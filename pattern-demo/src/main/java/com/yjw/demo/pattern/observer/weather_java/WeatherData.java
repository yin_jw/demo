package com.yjw.demo.pattern.observer.weather_java;


import java.util.Observable;

/**
 * 气象站数据
 */
public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    /**
     * 从气象站得到更新观测值，通知观察者
     */
    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    /**
     * 设置气象数据
     *
     * @param temperature
     * @param humidity
     * @param pressure
     */
    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }
}
