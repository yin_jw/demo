package com.yjw.demo.pattern.proxy2.jdk;

import com.yjw.demo.pattern.proxy2.staticed.Person;
import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;

public class JDKProxyTest {
    public static void main(String[] args) throws Exception {
        Person person = new JDKMeipo(new Customer()).getInstance();
        person.findLove();

        // 通过反编译工具可以查看源代码
        byte[] bytes = ProxyGenerator.generateProxyClass("$Proxy0", new Class[]{Person.class});
        FileOutputStream os = new FileOutputStream("/Users/yinjianwei/Downloads/Proxy.class");
        os.write(bytes);
        os.close();
    }
}
