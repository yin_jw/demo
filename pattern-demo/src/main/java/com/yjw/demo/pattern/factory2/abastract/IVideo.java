package com.yjw.demo.pattern.factory2.abastract;

public interface IVideo {
    void record();
}