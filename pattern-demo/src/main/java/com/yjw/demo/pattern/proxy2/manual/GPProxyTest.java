package com.yjw.demo.pattern.proxy2.manual;

import com.yjw.demo.pattern.proxy2.jdk.Customer;
import com.yjw.demo.pattern.proxy2.staticed.Person;

public class GPProxyTest {
    public static void main(String[] args) {
        Person person = new GPMeipo(new Customer()).getInstance();
        person.findLove();
    }
}
