package com.yjw.demo.pattern.factory.function;

import com.yjw.demo.pattern.factory.Milk;
import com.yjw.demo.pattern.factory.Sanlu;

public class SanluFactory implements Factory {

    @Override
    public Milk getMilk() {
        return new Sanlu();
    }

}
