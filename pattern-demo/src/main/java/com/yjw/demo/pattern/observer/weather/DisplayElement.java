package com.yjw.demo.pattern.observer.weather;

/**
 * 布告板显示
 */
public interface DisplayElement {

    /**
     * 显示气象数据
     */
    public void display();
}