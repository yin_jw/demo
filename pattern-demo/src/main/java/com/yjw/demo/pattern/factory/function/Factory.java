package com.yjw.demo.pattern.factory.function;

import com.yjw.demo.pattern.factory.Milk;

/**
 * 工厂方法模式
 * 
 * @author yinjianwei
 * @date 2018/12/01
 */
public interface Factory {
    /**
     * 生产牛奶
     * 
     * @return
     */
    Milk getMilk();
}
