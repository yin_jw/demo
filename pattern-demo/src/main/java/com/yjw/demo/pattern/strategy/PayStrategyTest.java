package com.yjw.demo.pattern.strategy;

public class PayStrategyTest {
    public static void main(String[] args) {
        Order order = new Order("1", "20191119163020300", 300.87);
        System.out.println(order.pay(PayStrategy.ALI_PAY));
    }
}