package com.yjw.demo.pattern.factory.abstr;

import com.yjw.demo.pattern.factory.Milk;

/**
 * 抽象工厂模式
 * 
 * @author yinjianwei
 * @date 2018/12/01
 */
public abstract class AbstractFactory {

    /**
     * 获得蒙牛牛奶
     * 
     * @return
     */
    public abstract Milk getMengniu();

    /**
     * 获得三鹿牛奶
     * 
     * @return
     */
    public abstract Milk getSanlu();

    /**
     * 获得伊利牛奶
     * 
     * @return
     */
    public abstract Milk getYili();
}
