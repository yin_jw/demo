package com.yjw.demo.pattern.singleton2;

public class ExectorThread implements Runnable {
    @Override
    public void run() {
        ThreadLocalSigleton singleton = ThreadLocalSigleton.getInstance();
        System.out.println(Thread.currentThread().getName() + ":" + singleton);
    }
}
