package com.yjw.demo.pattern.factory2.abastract;

public interface CourseFactory {
    INote createNote();	
    IVideo createVideo();	
}