package com.yjw.demo.pattern.template2;

public abstract class NetworkCourse {

    protected final void createCourse() {
        // 发布预习资料
        this.postPreResource();

        // 制作课件PPT
        this.createPPT();

        // 在线直播
        this.liveVideo();

        // 提交课堂笔记
        this.postNote();

        // 提交源码
        this.postSource();

        // 布置作业，有些课程是没有作业的，有些课程是有作业的
        // 如果有作业，检查作业，如果没有作业，流程结束
        if (needHomework()) {
            checkHomework();
        }
    }

    private void postPreResource() {
        System.out.println("发布预习资料");
    }

    private void createPPT() {
        System.out.println("制作课件PPT");
    }

    private void liveVideo() {
        System.out.println("在线直播");
    }

    private void postNote() {
        System.out.println("提交源代码");
    }

    private void postSource() {
        System.out.println("提交课件和笔记");
    }

    /**
     * 钩子方法：实现流程的微调
     *
     * @return
     */
    protected boolean needHomework() {
        return false;
    }

    protected abstract void checkHomework();
}