package com.yjw.demo.pattern.proxy2.template;

public interface Subject {
    public void request();
}
