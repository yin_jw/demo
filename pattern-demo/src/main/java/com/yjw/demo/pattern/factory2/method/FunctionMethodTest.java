package com.yjw.demo.pattern.factory2.method;

import com.yjw.demo.pattern.factory2.simple.ICourse;

public class FunctionMethodTest {
    public static void main(String[] args) {
        ICourseFactory factory = new PythonCourseFactory();
        ICourse course = factory.crete();
        course.record();

        factory = new JavaCourseFactory();
        course = factory.crete();
        course.record();
    }
}
