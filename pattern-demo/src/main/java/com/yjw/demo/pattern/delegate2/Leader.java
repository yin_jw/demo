package com.yjw.demo.pattern.delegate2;

import java.util.HashMap;
import java.util.Map;

public class Leader implements IEmployee {

    private Map<String, IEmployee> targets = new HashMap<>();

    public Leader() {
        targets.put("加密", new EmployeeA());
        targets.put("登录", new EmployeeA());
    }

    /**
     * 项目经理自己不干活，委派任务给对应的员工工作
     *
     * @param command
     */
    @Override
    public void doing(String command) {
        targets.get(command).doing(command);
    }
}