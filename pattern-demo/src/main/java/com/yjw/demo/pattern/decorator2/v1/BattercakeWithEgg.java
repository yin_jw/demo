package com.yjw.demo.pattern.decorator2.v1;

public class BattercakeWithEgg extends Battercake {

    @Override
    protected String getMsg() {
        return super.getMsg() + "+1个鸡蛋";
    }

    /**
     * 加1个鸡蛋加1元钱
     *
     * @return
     */
    @Override
    public int getPrice() {
        return super.getPrice() + 1;
    }
}