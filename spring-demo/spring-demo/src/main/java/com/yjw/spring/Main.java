package com.yjw.spring;

import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;
import com.yjw.spring.service.UserService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-context.xml");
        UserService userService = (UserService) context.getBean("userService");
        System.out.println("#####################");
        context.close();
    }
}
