package com.yjw.serial;

import com.yjw.serial.serializer.*;

public class SerialDemo {

    public static void main(String[] args) {
//        javaSerializerWithFile();
        javaSerializer();
    }

    /**
     * Java 原生写文件
     */
    public static void javaSerializerWithFile() {
        User user = new User("Yjw", 18);
        ISerializer iSerializer = new JavaSerializerWithFile();
        iSerializer.serialize(user);
//        iSerializer.deserialize(null, User.class);
    }

    /**
     * Java 原生序列化
     */
    public static void javaSerializer() {
        User user = new User("Yjw", 18);
        ISerializer iSerializer = new JavaSerializer();
        byte[] bytes = iSerializer.serialize(user);
        System.out.println("Java Seriallizer byte length: " + bytes.length);
        User userBySerial = iSerializer.deserialize(bytes, User.class);
        System.out.println(userBySerial);
    }

    /**
     * XStream 序列化
     */
    public static void xStreamSerializer() {
        User user = new User("Yjw", 18);
        ISerializer iSerializer = new XStreamSerializer();
        byte[] bytes = iSerializer.serialize(user);
        System.out.println("XStream Seriallizer byte length: " + bytes.length);
        User userBySerial = iSerializer.deserialize(bytes, User.class);
        System.out.println(userBySerial);
    }

    /**
     * JSON 序列化
     */
    public static void fastJsonSerializer() {
        User user = new User("Yjw", 18);
        ISerializer iSerializer = new FastJsonSerializer();
        byte[] bytes = iSerializer.serialize(user);
        System.out.println("JSON Seriallizer byte length: " + bytes.length);
        User userBySerial = iSerializer.deserialize(bytes, User.class);
        System.out.println(userBySerial);
    }

    /**
     * Hessian 序列化
     */
    public static void hessianSerializer() {
        User user = new User("Yjw", 18);
        ISerializer iSerializer = new HessianSerializer();
        byte[] bytes = iSerializer.serialize(user);
        System.out.println("Hessian Seriallizer byte length: " + bytes.length);
        User userBySerial = iSerializer.deserialize(bytes, User.class);
        System.out.println(userBySerial);
    }


}