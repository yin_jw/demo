package com.yjw.serial.serializer;

import java.io.*;

/**
 * @author: James Yin
 * @description:
 * @date: 2/2/2020 5:46 PM
 */
public class JavaSerializerWithFile implements ISerializer {

    @Override
    public <T> byte[] serialize(T obj) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream(new File(obj.getClass().getSimpleName())));
            oos.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    @Override
    public <T> T deserialize(byte[] data, Class<T> clazz) {
        try {
            ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(new File(clazz.getSimpleName())));
            return (T) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
