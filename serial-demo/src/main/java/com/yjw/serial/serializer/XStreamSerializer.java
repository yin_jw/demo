package com.yjw.serial.serializer;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * @author: James Yin
 * @description:
 * @date: 2/2/2020 5:46 PM
 */
public class XStreamSerializer implements ISerializer {

    private static XStream xStream = new XStream(new DomDriver());

    @Override
    public <T> byte[] serialize(T obj) {
        return xStream.toXML(obj).getBytes();
    }

    @Override
    public <T> T deserialize(byte[] data, Class<T> clazz) {
        return (T) xStream.fromXML(new String(data));
    }
}