package com.yjw.serial.serializer;

import com.alibaba.fastjson.JSON;

/**
 * @author: James Yin
 * @description:
 * @date: 2/2/2020 5:46 PM
 */
public class FastJsonSerializer implements ISerializer {

    @Override
    public <T> byte[] serialize(T obj) {
        return JSON.toJSONString(obj).getBytes();
    }

    @Override
    public <T> T deserialize(byte[] data, Class<T> clazz) {
        return JSON.parseObject(new String(data), clazz);
    }
}
