package com.yjw.serial;


public class ProtobufDemo {

    public static void main(String[] args) {
        UserProtos.User user = UserProtos.User.newBuilder().
                setAge(300).setName("Yjw").build();
        byte[] bytes = user.toByteArray();
        for (int i = 0; i < bytes.length; i++) {
            System.out.print(bytes[i] + " ");
        }
        // 10 3 89 106 119 16 -84 2
    }
}
